// install (please make sure versions match peerDependencies)
// yarn add @nivo/core @nivo/line
import React from "react";
import { ResponsiveLine } from "@nivo/line";
import Moment from "react-moment";
// make sure parent container have a defined height when using
// responsive component, otherwise height will be 0 and
// no chart will be rendered.
// website examples showcase many properties,
// you'll often use just a few of them.
const LineAreaGradientChart = ({
  data,
  maxMin,
  valueUnitAbbrev,
  baselineValue,
}) => {
  const CustomSymbol = ({ size, color, borderWidth, borderColor }) => (
    <g>
      <circle
        fill="#fff"
        r={size / 2}
        strokeWidth={borderWidth}
        stroke={borderColor}
      />
      <circle
        r={size / 5}
        strokeWidth={borderWidth}
        stroke={borderColor}
        fill={color}
        fillOpacity={0.35}
      />
    </g>
  );

  const CommonTooltipProperties = {
    background: "white",
    padding: "9px 12px",
    border: "1px solid #ccc",
    borderRadius: 15,
    maxWidth: "auto",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
  };

  const commonStackedProperties = {
    type: "linear",
    min: "auto",
    max: "auto",
    stacked: false,
    reverse: false,
  };

  const CommonGradientProperties = [
    {
      id: "gradientCategoryMain",
      type: "linearGradient",
      colors: [
        { offset: 10, color: "inherit" },
        { offset: 90, color: "#ffff" },
      ],
    },
  ];

  const CommonGraphProperties = {
    yFormat: " >-.2f",
    crosshairType: "cross",
    pointSize: "16",
    pointBorderWidth: "1",
    enableStackTooltip: true,
    margin: { top: 3, right: 0, bottom: 3, left: 0 },
    enableGridX: true,
    enableGridY: false,
    enableSlices: "x",
    enableArea: true,
    areaOpacity: "0.07",
    useMesh: true,
  };

  const CompareStationBaseline = [
    {
      axis: "y",
      value: baselineValue,
      lineStyle: { stroke: "#b0413e", strokeWidth: 3 },
      legend: "baseline",
      legendOrientation: "horizontal",
    },
  ];

  const MaxMinBaseline = [
    {
      axis: "y",
      value: baselineValue,
      lineStyle: { stroke: "#f0f0f0", strokeWidth: 0 },
      legend: "baseline",
      legendOrientation: "horizontal",
    },
  ];

  //data coming in LineAreaGradientChart component
  console.log("data LineAreaGradientChart ", data);

  return maxMin ? (
    <ResponsiveLine
      {...CommonGraphProperties}
      data={data}
      xScale={{ type: "point" }}
      yScale={{ ...commonStackedProperties }}
      pointSymbol={CustomSymbol}
      pointBorderColor={{
        from: "color",
        modifiers: [["darker", 0.3]],
      }}
      colors={["#ffa900", "#50D050", "#ff0145"]}
      markers={[...CompareStationBaseline]}
      sliceTooltip={({ slice }) => {
        return (
          <div style={{ ...CommonTooltipProperties }}>
            {
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <strong>
                  {
                    <Moment
                      date={slice.points[0].data.x}
                      format="dddd, MMMM Do"
                      local
                    ></Moment>
                  }
                </strong>{" "}
                <i>({valueUnitAbbrev}/day)</i>
              </div>
            }
            {
              <div style={{ color: "#ff0145" }}>
                Max: <strong>{slice.points[0].data.yFormatted}</strong>{" "}
              </div>
            }
            {
              <div style={{ color: "#008000" }}>
                Avg: <strong>{slice.points[1].data.yFormatted}</strong>{" "}
              </div>
            }
            {
              <div style={{ color: "#ffa200" }}>
                Min: <strong>{slice.points[2].data.yFormatted}</strong>
              </div>
            }
          </div>
        );
      }}
      defs={[...CommonGradientProperties]}
      fill={[{ match: "*", id: "gradientCategoryMain" }]}
    />
  ) : (
    <ResponsiveLine
      {...CommonGraphProperties}
      data={data}
      xScale={{ type: "point" }}
      yScale={{ ...commonStackedProperties }}
      yFormat=" >-.0f"
      pointSymbol={CustomSymbol}
      pointBorderColor={{
        from: "color",
        modifiers: [["darker", 0.3]],
      }}
      colors={["#ffa900", "#50D050", "#ff0145"]}
      markers={[...MaxMinBaseline]}
      defs={[...CommonGradientProperties]}
      fill={[{ match: "*", id: "gradientCategoryMain" }]}
      axisBottom={{
        orient: "bottom",
        tickSize: 5,
        tickPadding: 5,
      }}
      axisLeft={null}
      margin={{ top: 8, right: 15, bottom: 23, left: 15 }}
      sliceTooltip={({ slice }) => {
        return (
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              justifyContent: "center",
              background: "white",
              padding: "9px 12px",
              border: "1px solid #ccc",
            }}
          >
            {slice.points.map((point) => (
              <div
                key={point.id}
                style={{
                  color: point.serieColor,
                  padding: "3px 0",
                }}
              >
                <strong>{point.serieId}</strong>:{" "}
                {valueUnitAbbrev !== "£"
                  ? point.data.yFormatted + " " + valueUnitAbbrev
                  : valueUnitAbbrev + point.data.yFormatted}
              </div>
            ))}
          </div>
        );
      }}
    />
  );
};

export default LineAreaGradientChart;
