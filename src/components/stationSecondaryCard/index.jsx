import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  Card,
  Menu,
  MenuItem,
  CardContent,
  Button,
  Typography,
  Grid,
} from "@material-ui/core";

import MonetizationOnIcon from "@material-ui/icons/MonetizationOn";
import TrendingUpIcon from "@material-ui/icons/TrendingUp";
import TrendingDownIcon from "@material-ui/icons/TrendingDown";
import { Link, GradientAreaLine, LineAreaGradientChart } from "components";
import Moment from "react-moment";
import "moment-timezone";

import MoreVertIcon from "@material-ui/icons/MoreVert";
const StationSecondaryCard = ({ stationCategoryData }) => {
  const classes = useStyles();

  const [anchorEl, setAnchorEl] = React.useState(null);
  console.log("stationCategoryData", stationCategoryData);
  console.log("minGraphData", stationCategoryData.minGraphData);
  const allGraphs = stationCategoryData?.minGraphData?.concat(
    stationCategoryData.graphData,
    stationCategoryData.maxGraphData
  );
  console.log("allGraphs", allGraphs);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <Card
      className={classes.root}
      style={{ border: "none", boxShadow: "none" }}
    >
      <Typography color="textSecondary" align="right">
        <Button
          aria-controls="simple-menu"
          aria-haspopup="true"
          onClick={handleClick}
        >
          <MoreVertIcon />
        </Button>
        <Menu
          id="card-menu"
          anchorEl={anchorEl}
          keepMounted
          open={Boolean(anchorEl)}
          onClose={handleClose}
        >
          <MenuItem component={Link} href="/station/[id]" as={`/station/1`}>
            See More
          </MenuItem>
          <MenuItem onClick={handleClose}>Export</MenuItem>
          <MenuItem onClick={handleClose}>Delete</MenuItem>
        </Menu>
      </Typography>
      <CardContent style={{ padding: 10 }}>
        <Typography
          className={classes.title}
          color="textSecondary"
          gutterBottom
        >
          {stationCategoryData.name}
        </Typography>
      </CardContent>
      <Grid style={{ height: 250, margin: "15px -15px 15px -15px" }}>
        {/* <GradientAreaLine graphData={stationCategoryData.graphData} /> */}
        <LineAreaGradientChart
          data={allGraphs}
          maxMin={true}
          valueUnitAbbrev={stationCategoryData.measureUnitAbbrev}
          baselineValue={stationCategoryData.avgValue}
        />
      </Grid>
      <CardContent style={{ padding: 10 }}>
        <Typography variant="h2" component="h2" className={classes.figure}>
          {stationCategoryData.valueUnit}
          {stationCategoryData.measureUnitAbbrev === "people"
            ? Math.round(stationCategoryData.avgValue?.toFixed(2))
            : stationCategoryData.avgValue?.toFixed(2)}
          <Typography variant="h6" gutterBottom>
            {stationCategoryData.measureUnitAbbrev === "m"
              ? " "
              : stationCategoryData.measureUnitAbbrev}
          </Typography>
        </Typography>
        <Grid container>
          <Grid item xs={12}>
            <Grid item xs={6}>
              <Typography variant="body2" className={classes.wrapIcon}>
                <TrendingUpIcon className={classes.spaceRight} /> Max:
              </Typography>{" "}
            </Grid>
            <Grid item xs={6}>
              <Typography variant="body2" className={classes.wrapIcon}>
                <TrendingDownIcon className={classes.spaceRight} /> Min:
              </Typography>{" "}
            </Grid>
            <Grid item xs={12}>
              <Typography variant="body2" className={classes.wrapIcon}>
                <MonetizationOnIcon className={classes.spaceRight} /> Benefits
                realised
              </Typography>{" "}
            </Grid>
          </Grid>
        </Grid>
        {stationCategoryData.updatedAt == undefined ? (
          " "
        ) : (
          <Typography
            variant="body2"
            className={classes.updated}
            color="textSecondary"
          >
            {"Last updated: "}
            <Moment
              date={stationCategoryData?.updatedAt}
              format="ddd, MMMM Do YYYY"
              local
            ></Moment>{" "}
            (<Moment to={stationCategoryData.updatedAt} />)
          </Typography>
        )}
      </CardContent>
    </Card>
  );
};

const useStyles = makeStyles((theme) => ({
  root: {
    // maxWidth: 300,
    maxWidth: "100%",
    height: "100%",
    backgroundColor: "#fff",
    padding: "15px 15px",
  },

  title: {
    fontSize: 18,
    color: "#939393",
    fontWeight: 800,
    marginBottom: 15,
  },
  updated: {
    marginTop: 48,
    fontWeight: 400,
    color: "#939393",
  },
  spaceRight: {
    marginRight: theme.spacing(2),
  },
  wrapIcon: {
    verticalAlign: "text-top",
    display: "inline-flex",
  },
  figure: {
    display: "flex",
    alignItems: "flex-end",
    fontSize: 50,
    fontWeight: 800,
    color: "#E7843C",
  },
}));

export default StationSecondaryCard;
