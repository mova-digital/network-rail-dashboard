import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  Grid,
  Typography,
  FormControl,
  Select,
  MenuItem,
  Button,
  Input,
  ListItemText,
  Checkbox,
} from "@material-ui/core";
import { OptionsContext } from "./provider";
import { DatePicker } from "components";

const OptionsBar = ({ initialCat, ctg, station }) => {
  const classes = useStyles();
  const options = React.useContext(OptionsContext);

  const checkTwoStations = (station1, station2) => {
    if (station1 == station2) return false;
    else return true;
  };
  
  const stationNames = ["Chadwell Heath", "Waterloo", "Gidea Park"];
  const areaNames = ["Area 1", "Area 2", "Area 3"];

  return (
    <Grid container spacing={1} className={classes.root}>
      {ctg ? (
        <>
          <Grid item>
            <Typography gutterBottom variant="button" component="h6">
              Select stations:{" "}
            </Typography>
            <FormControl
              variant="outlined"
              className={classes.formControlStation}
            >
              <Select
                className={classes.formControlStation}
                labelId="select-stations"
                id="select-stations"
                displayEmpty
                multiple
                value={options.stations}
                renderValue={() =>
                  options.stations.length === 0 ? (
                    <em>Select at least two stations</em>
                  ) : (
                    (options.stations as string[]).join(", ")
                  )
                }
                onChange={options.handleStationsChange}
              >
                <MenuItem disabled>
                  <em>Please select at least two stations</em>
                </MenuItem>
                {stationNames.map((name) => (
                  <MenuItem key={name} value={name}>
                    <Checkbox
                      checked={
                        true ? options.stations.indexOf(name) > -1 : false
                      }
                    />
                    <ListItemText primary={name} />
                  </MenuItem>
                ))}
              </Select>
            </FormControl>{" "}
          </Grid>
          <Grid item>
            <Typography gutterBottom variant="button" component="h6">
              Select areas:{" "}
            </Typography>
            <FormControl
              variant="outlined"
              className={classes.formControlAreas}
            >
              <Select
                className={classes.formControlAreas}
                labelId="select-areas"
                id="select-areas"
                displayEmpty
                disabled={options.stations.length >= 2 ? true : false}
                multiple
                value={options.areas}
                renderValue={() =>
                  options.areas.length === 0 ? (
                    <em>Select at least two areas</em>
                  ) : (
                    (options.areas as string[]).join(", ")
                  )
                }
                onChange={options.handleAreasChange}
              >
                <MenuItem disabled>
                  <em>Please select at least two areas</em>
                </MenuItem>
                {areaNames.map((name) => (
                  <MenuItem key={name} value={name}>
                    <Checkbox checked={options.areas.indexOf(name) > -1} />
                    <ListItemText primary={name} />
                  </MenuItem>
                ))}
              </Select>
            </FormControl>{" "}
          </Grid>
        </>
      ) : station ? (
        <Grid item>
          <Typography gutterBottom variant="button" component="h6">
            Select station:{" "}
          </Typography>
          <FormControl
            variant="outlined"
            disabled
            className={classes.formControlStation}
          >
            <Select
              displayEmpty
              renderValue={() =>
                !options.firstLocation ? (
                  <em>Please select one station</em>
                ) : (
                  options.firstLocation
                )
              }
              labelId="select-first-location"
              id="select-first-location"
              value={options.firstLocation}
              onChange={options.handleFirstLocationChange}
            >
              <MenuItem disabled value="">
                <em>Please select a station</em>
              </MenuItem>
              <MenuItem value="Waterloo (LDN)">Waterloo (LDN)</MenuItem>
              <MenuItem value="Paddington (LDN)">Paddington (LDN)</MenuItem>
              <MenuItem value="Newstreet (MNC)">Newstreet (MNC)</MenuItem>
            </Select>
          </FormControl>{" "}
        </Grid>
      ) : (
        <Grid item>
          <Typography gutterBottom variant="button" component="h6">
            Select stations:{" "}
          </Typography>
          <FormControl variant="outlined" className={classes.formControl}>
            <Select
              displayEmpty
              renderValue={() =>
                !options.firstLocation ? (
                  <em>Please select first station</em>
                ) : (
                  options.firstLocation
                )
              }
              labelId="select-first-location"
              id="select-first-location"
              value={options.firstLocation}
              onChange={options.handleFirstLocationChange}
            >
              <MenuItem disabled value="">
                <em>Please select first station</em>
              </MenuItem>
              <MenuItem
                disabled={
                  !checkTwoStations("Waterloo (LDN)", options.secondLocation)
                }
                value="Waterloo (LDN)"
              >
                Waterloo (LDN)
              </MenuItem>
              <MenuItem
                disabled={
                  !checkTwoStations("Paddington (LDN)", options.secondLocation)
                }
                value="Paddington (LDN)"
              >
                Paddington (LDN)
              </MenuItem>
              <MenuItem
                disabled={
                  !checkTwoStations("Newstreet (MNC)", options.secondLocation)
                }
                value="Newstreet (MNC)"
              >
                Newstreet (MNC)
              </MenuItem>
            </Select>
          </FormControl>{" "}
          <FormControl variant="outlined" className={classes.formControl}>
            <Select
              displayEmpty
              renderValue={() =>
                !options.secondLocation ? (
                  <em>Please select second station</em>
                ) : (
                  options.secondLocation
                )
              }
              labelId="select-second-location"
              id="select-second-location"
              value={options.secondLocation}
              onChange={options.handleSecondLocationChange}
            >
              <MenuItem disabled value="">
                <em>Please select second station</em>
              </MenuItem>
              <MenuItem
                disabled={
                  !checkTwoStations(options.firstLocation, "Waterloo (LDN)")
                }
                value="Waterloo (LDN)"
              >
                Waterloo (LDN)
              </MenuItem>
              <MenuItem
                disabled={
                  !checkTwoStations(options.firstLocation, "Paddington (LDN)")
                }
                value="Paddington (LDN)"
              >
                Paddington (LDN)
              </MenuItem>
              <MenuItem
                disabled={
                  !checkTwoStations(options.firstLocation, "Newstreet (MNC)")
                }
                value="Newstreet (MNC)"
              >
                Newstreet (MNC)
              </MenuItem>
            </Select>
          </FormControl>
        </Grid>
      )}
      <Grid item>
        <Typography variant="button" component="h6">
          Select time frame:{" "}
        </Typography>
        <DatePicker />
      </Grid>
      <Grid
        item
        style={{
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "center",
          margin: 5,
        }}
      >
        <Button
          disableElevation
          variant="contained"
          color="primary"
          style={{ color: "white" }}
        >
          {"Search"}
        </Button>{" "}
      </Grid>
    </Grid>
  );
};

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: 10,
    padding: 15,
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: "white",
    borderRadius: 15,
  },
  formControl: {
    width: 200,
  },
  formControlStation: {
    width: 250,
  },
  formControlAreas: {
    width: 150,
  },
}));

export default OptionsBar;
