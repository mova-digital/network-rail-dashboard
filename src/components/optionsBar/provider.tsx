import React from "react";

interface OptionsContextInterface {
  firstLocation: string;
  secondLocation: string;
  category: string;
  stations: string[];
  areas: string[];
  handleFirstLocationChange: any;
  handleSecondLocationChange: any;
  handleCategoryChange: any;
  handleStationsChange: any;
  handleAreasChange: any;
  handleStationChipRemove: any;
  handleAreaChipRemove: any;
}

export const OptionsContext =
  React.createContext<OptionsContextInterface | null>(null);

export const OptionsProvider = ({ children }) => {
  const [firstLocation, setFirstLocation] = React.useState("");
  const [secondLocation, setSecondLocation] = React.useState("");
  const [category, setCategory] = React.useState("");
  const [stations, setStations] = React.useState<string[]>([]);
  const [areas, setAreas] = React.useState<string[]>([]);

  const handleFirstLocationChange = (event) => {
    setFirstLocation(event.target.value);
  };

  const handleSecondLocationChange = (event) => {
    setSecondLocation(event.target.value);
  };

  const handleCategoryChange = (event) => {
    setCategory(event.target.value);
  };

  const handleStationsChange = (
    event: React.ChangeEvent<{ value: unknown }>
  ) => {
    setStations(event.target.value as string[]);
  };

  const handleAreasChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    setAreas(event.target.value as string[]);
  };

  const handleStationChipRemove = (stationName) => {
    const newStations = stations.filter((station) => station !== stationName);
    setStations(newStations);
  };

  const handleAreaChipRemove = (areaName) => {
    const newAreas = areas.filter((area) => area !== areaName);
    setAreas(newAreas);
  };
  return (
    <OptionsContext.Provider
      value={{
        firstLocation,
        secondLocation,
        category,
        stations,
        areas,
        handleFirstLocationChange,
        handleSecondLocationChange,
        handleCategoryChange,
        handleStationsChange,
        handleAreasChange,
        handleStationChipRemove,
        handleAreaChipRemove,
      }}
    >
      {children}
    </OptionsContext.Provider>
  );
};
