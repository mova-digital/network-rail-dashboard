import React, { useContext } from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import {
  Container,
  Grid,
  Card,
  CardContent,
  Button,
  Typography,
  CardActions,
} from "@material-ui/core";
import Lottie from "react-lottie";
import restrictedLock from "../../../public/lotties/orange_lock.json";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import orangeLoading from "../../../public/lotties/orange_loading.json";
import { AuthContext } from "../../contexts/AuthContext";
import Image from "next/image";
import SaabreLogo from "../../../public/images/saabrelogo.png";

const defaultCardOptions = {
  loop: true,
  autoplay: true,
  animationData: restrictedLock,
  rendererSettings: {
    preserveAspectRatio: "xMidYMid slice",
  },
};
const defaultLoadingOptions = {
  loop: true,
  autoplay: true,
  animationData: orangeLoading,
  rendererSettings: {
    preserveAspectRatio: "xMidYMid slice",
  },
};

const RestricedAreaCard = ({ loading }) => {
  const { requestSignIn } = useContext(AuthContext);
  const classes = useStyles();

  return (
    <Container>
      <Grid item className={classes.root}>
        <Card className={classes.cardRoot} variant="outlined">
          <CardContent className={classes.cardContent}>
            <Grid container>
              <Grid item xs={12}>
                <Lottie options={defaultCardOptions} width={300} height={170} />
              </Grid>
              <Grid item xs={12} className={classes.cardContent}>
                <Image
                  src={SaabreLogo}
                  alt="Saabre picture"
                  priority={true}
                  width={250}
                  height={190}
                />
              </Grid>
            </Grid>
          </CardContent>
          {loading ? (
            <CardActions className={classes.cardActions}>
              <Typography
                className={classes.title}
                color="textSecondary"
                gutterBottom
              >
                Logging you in...
              </Typography>
              <Lottie options={defaultLoadingOptions} width={400} />
            </CardActions>
          ) : (
            <CardActions className={classes.cardActions}>
              <Typography
                className={classes.title}
                color="textSecondary"
                gutterBottom
              >
                You need to login to view this page.
              </Typography>
              <Button
                color="secondary"
                className={classes.button}
                onClick={requestSignIn}
                variant="outlined"
                startIcon={<ExitToAppIcon />}
              >
                Login with SAaBRE oAuth2
              </Button>
            </CardActions>
          )}
        </Card>
      </Grid>
    </Container>
  );
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: "flex",
      alignItems: "flex-end",
      justifyContent: "center",
      marginTop: "8vh",
    },
    cardRoot: {
      width: "50vw",
      height: "70vh",
      marginTop: "5vh",
      marginBottom: "5vh",
      borderRadius: 50,
    },
    bullet: {
      display: "inline-block",
      margin: "0 2px",
      transform: "scale(0.8)",
    },
    title: {
      fontSize: 18,
      marginBottom: 15,
    },
    pos: {
      marginBottom: 12,
    },
    cardActions: {
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
      justifyContent: "center",
    },
    button: {
      margin: theme.spacing(1),
    },
    cardContent: {
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
    },
  })
);

export default RestricedAreaCard;
