// yarn add @nivo/core @nivo/line
import React from "react";
import { ResponsiveLine } from "@nivo/line";
import Moment from "react-moment";
// make sure parent container have a defined height when using
// responsive component, otherwise height will be 0 and
// no chart will be rendered.

const GradientAreaLine = ({ graphData, valueUnitAbbrev }) => {
  {
    console.log(valueUnitAbbrev);
  }
  return (
    <ResponsiveLine
      isInteractive
      data={graphData}
      enableArea={true}
      useMesh={true}
      areaOpacity={0.8}
      colors={{ scheme: "oranges" }}
      lineWidth={10}
      enableStackTooltip
      enableGridX={false}
      enableGridY={false}
      margin={{ top: 0, right: 0, bottom: 0, left: 0 }}
      xScale={{ type: "point" }}
      yScale={{
        type: "linear",
        min: 0,
        max: "auto",
        stacked: true,
      }}
      yFormat=" >-.2f"
      defs={[
        {
          id: "gradientInsights",
          type: "linearGradient",
          colors: [
            { offset: 30, color: "#ffa400" },
            { offset: 100, color: "#ffff" },
          ],
        },
      ]}
      fill={[{ match: "*", id: "gradientInsights" }]}
      fillOpacity={0.8}
      //costum tooltip to show info on hover
      enableSlices="x"
      sliceTooltip={({ slice }) => {
        return (
          <div
            style={{
              background: "white",
              padding: "9px 12px",
              border: "1px solid #ccc",
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            {
              <div>
                <strong>
                  {
                    <Moment
                      date={slice.points[0].data.x}
                      format="dddd, MMMM Do"
                      local
                    ></Moment>
                  }
                </strong>
              </div>
            }

            {slice.points.map((point) => (
              <>
                <div
                  key={point.data.x}
                  style={{
                    color: "#E7843C",
                    padding: "3px 0",
                  }}
                >
                  <strong>
                    {valueUnitAbbrev === " "
                      ? "£" + point.data.y.toFixed(2)
                      : point.data.y.toFixed(2) + " " + valueUnitAbbrev}
                  </strong>
                </div>
              </>
            ))}
          </div>
        );
      }}
    />
  );
};

export default GradientAreaLine;
