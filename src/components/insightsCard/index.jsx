import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  Card,
  Menu,
  MenuItem,
  CardContent,
  Button,
  Typography,
  Grid,
  Chip,
} from "@material-ui/core";
import { Link, GradientAreaLine } from "components";
import Moment from "react-moment";
import "moment-timezone";

import MoreVertIcon from "@material-ui/icons/MoreVert";

const InsightsCard = ({ data }) => {
  const classes = useStyles();

  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  console.log("InsightsCard: ", data);

  return (
    <Card
      key={data.id}
      className={classes.root}
      style={{ border: "none", boxShadow: "none" }}
    >
      <Typography color="textSecondary" align="right">
        <Button
          aria-controls="simple-menu"
          aria-haspopup="true"
          onClick={handleClick}
        >
          <MoreVertIcon />
        </Button>
        <Menu
          id="card-menu"
          anchorEl={anchorEl}
          keepMounted
          open={Boolean(anchorEl)}
          onClose={handleClose}
        >
          <MenuItem
            component={Link}
            href="/category/[cat]"
            as={`/category/${data.name}`}
          >
            See More
          </MenuItem>
          <MenuItem onClick={handleClose}>Export</MenuItem>
        </Menu>
      </Typography>
      <CardContent style={{ padding: 10 }}>
        <Typography
          className={classes.title}
          color="textSecondary"
          gutterBottom
        >
          {data?.name?.toUpperCase()}{" "}
          <Typography
            variant="body2"
            className={classes.updated}
            color="textSecondary"
            style={{ paddingBottom: 10 }}
          >
            {" (per/week)"}
          </Typography>
        </Typography>

        <Typography variant="h2" component="h2" className={classes.figure}>
          {data.valueUnit}
          {data.measureUnitAbbrev === "people"
            ? Math.round(data.avgValue?.toFixed(2))
            : data.avgValue?.toFixed(2)}
          <Typography variant="h6" gutterBottom>
            {data.measureUnitAbbrev === "m"
              ? (data.measureUnitAbbrev = " ")
              : data.measureUnitAbbrev}
          </Typography>
        </Typography>

        <Typography variant="h6" component="h6">
          <Chip
            // color={
            //   Number(data.avgValTrendPercentage?.toFixed(2)) < 0
            //     ? "secondary"
            //     : "primary"
            // }
            style={
              Number(data.peopleFootfallTrend) >= 0
                ? { backgroundColor: "#ef5350", color: "white" }
                : { backgroundColor: "#50D050", color: "white" }
            }
            label={
              data.avgValTrendPercentage?.toFixed(2) > 1
                ? data.avgValTrendPercentage?.toFixed(2) +
                  "% (+" +
                  Math.round(data.avgValTrend?.toFixed(2)) +
                  data.measureUnitAbbrev +
                  ")"
                : data.avgValTrendPercentage?.toFixed(2) +
                  "% (" +
                  data.avgValTrend?.toFixed(2) +
                  data.measureUnitAbbrev +
                  ")"
            }
            size="small"
          />
        </Typography>
        {data.updatedAt == undefined ? (
          <div style={{ marginBottom: 15 }} />
        ) : (
          <Typography
            variant="body2"
            className={classes.updated}
            color="textSecondary"
            style={{ paddingBottom: 10 }}
          >
            {"Last updated: "}
            <Moment
              date={data?.updatedAt}
              format="dddd, MMMM Do"
              local
            ></Moment>{" "}
            (<Moment to={data.updatedAt} />)
          </Typography>
        )}
      </CardContent>
      <Grid style={{ height: 250, margin: -15 }}>
        <GradientAreaLine
          graphData={data.graphData}
          valueUnitAbbrev={data.measureUnitAbbrev}
        />
      </Grid>
    </Card>
  );
};

const useStyles = makeStyles({
  root: {
    // maxWidth: 300,
    maxWidth: "100%",
    backgroundColor: "#fff",
    padding: "15px 15px",
  },
  title: {
    fontSize: 14,
    color: "#939393",
    fontWeight: 800,
  },

  figure: {
    display: "flex",
    alignItems: "flex-end",
    fontSize: 50,
    fontWeight: 800,
    color: "#E7843C",
  },
  updated: {
    marginTop: 12,
    fontWeight: 400,
    color: "#939393",
  },
});

export default InsightsCard;
