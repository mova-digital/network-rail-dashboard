import "date-fns";
import React from "react";
import { Grid } from "@material-ui/core";
import DateFnsUtils from "@date-io/date-fns";
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker,
} from "@material-ui/pickers";

export default function DatePicker() {
  // The first commit of Material-UI
  const [selectedInitialDate, setSelectedInitialDate] =
    React.useState<Date | null>(new Date("2021-07-06T21:11:54"));
  const [selectedEndDate, setSelectedEndDate] = React.useState<Date | null>(
    new Date("2021-07-07T21:11:54")
  );

  const handleInitialDateChange = (date: Date | null) => {
    setSelectedInitialDate(date);
  };
  const handleEndDateChange = (date: Date | null) => {
    setSelectedEndDate(date);
  };

  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <Grid container spacing={1}>
        <Grid item xs={5}>
          <KeyboardDatePicker
            variant="dialog"
            inputVariant="outlined"
            format="dd/MM/yyyy"
            margin="normal"
            id="start-date-picker-inline"
            value={selectedInitialDate}
            onChange={handleInitialDateChange}
            KeyboardButtonProps={{
              "aria-label": "change start date",
            }}
          />
        </Grid>

        <Grid
          item
          xs={1}
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            paddingTop: 10,
          }}
        >
          {"to"}
        </Grid>
        <Grid item xs={5}>
          <KeyboardDatePicker
            variant="dialog"
            inputVariant="outlined"
            format="dd/MM/yyyy"
            margin="normal"
            id="end-date-picker-inline"
            value={selectedEndDate}
            onChange={handleEndDateChange}
            KeyboardButtonProps={{
              "aria-label": "change end date",
            }}
          />
        </Grid>
      </Grid>
    </MuiPickersUtilsProvider>
  );
}
