import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  Card,
  Menu,
  MenuItem,
  CardContent,
  Button,
  Typography,
  Grid,
  Chip,
} from "@material-ui/core";
import { Link, StationPieChart } from "components";

import MoreVertIcon from "@material-ui/icons/MoreVert";
import WebIcon from "@material-ui/icons/Web";
import PeopleAltIcon from "@material-ui/icons/PeopleAlt";
import MonetizationOnIcon from "@material-ui/icons/MonetizationOn";
import VisibilityIcon from "@material-ui/icons/Visibility";

const pieData = [
  {
    id: "no mask",
    label: "no mask",
    value: 20,
    color: "hsl(0, 0%, 90%)",
  },
  {
    id: "mask",
    label: "mask",
    value: 80,
    color: "hsl(25, 90%, 80%)",
  },
];

const StationMainCard = ({ stationCategoryData, stationData }) => {
  const classes = useStyles();

  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <Card
      className={classes.root}
      style={{ border: "none", boxShadow: "none" }}
    >
      <Typography color="textSecondary" align="right">
        <Button
          aria-controls="simple-menu"
          aria-haspopup="true"
          onClick={handleClick}
        >
          <MoreVertIcon />
        </Button>
        <Menu
          id="card-menu"
          anchorEl={anchorEl}
          keepMounted
          open={Boolean(anchorEl)}
          onClose={handleClose}
        >
          <MenuItem component={Link} href="/station/[id]" as={`/station/1`}>
            See More
          </MenuItem>
          <MenuItem onClick={handleClose}>Export</MenuItem>
          <MenuItem onClick={handleClose}>Delete</MenuItem>
        </Menu>
      </Typography>
      <CardContent style={{ padding: 10 }}>
        <Grid container>
          {stationCategoryData.map((category) => (
            <Grid item xs={3}>
              <Typography
                className={classes.title}
                color="textSecondary"
                gutterBottom
              >
                {category.name}
              </Typography>
              <Typography
                variant="h2"
                component="h2"
                className={classes.figureSmall}
              >
                {category.avgValue.toFixed(2)}
              </Typography>
            </Grid>
          ))}
        </Grid>
      </CardContent>
      <Grid container spacing={2}>
        <Grid item>
          <Typography variant="body2" className={classes.wrapIcon}>
            <WebIcon className={classes.spaceRight} />{" "}
            {stationData.advertisingZones} Advertising Zones
          </Typography>{" "}
        </Grid>
        <Grid item xs={9}>
          <Typography variant="body2" className={classes.wrapIcon}>
            <PeopleAltIcon className={classes.spaceRight} />{" "}
            {stationData.peopleFootfall} people footfall{" "}
            <Chip
              color={
                Number(stationData.peopleFootfallTrend) < 0
                  ? "secondary"
                  : "primary"
              }
              className={classes.spaceChip}
              label={
                stationData.peopleFootfallTrend == undefined
                  ? ""
                  : stationData.peopleFootfallTrend > 0
                  ? "+" + stationData.peopleFootfallTrend?.toFixed(2) + "%"
                  : "" + stationData.peopleFootfallTrend?.toFixed(2) + "%"
              }
              size="small"
            />
          </Typography>{" "}
        </Grid>

        <Grid item xs={5}>
          <Typography variant="body1" className={classes.wrapIcon}>
            <MonetizationOnIcon className={classes.spaceRight} /> £
            {stationData.benefitsRealised.toFixed(2)} Benefits Realised{" "}
            <Chip
              color={
                Number(stationData.benefitsRealisedTrend) < 0
                  ? "secondary"
                  : "primary"
              }
              className={classes.spaceChip}
              label={
                stationData.benefitsRealisedTrend == undefined
                  ? ""
                  : stationData.benefitsRealisedTrend >= 0.1
                  ? "+" + stationData.benefitsRealisedTrend?.toFixed(2) + "%"
                  : "" + stationData.benefitsRealisedTrend?.toFixed(2) + "%"
              }
              size="small"
            />
          </Typography>{" "}
        </Grid>
      </Grid>

      <Grid container>
        <Grid item xs={4} style={{ height: 250 }}>
          <StationPieChart data={pieData} />
        </Grid>
        <Grid item xs={4} style={{ height: 250 }}>
          <StationPieChart data={pieData} />
        </Grid>
        <Grid item xs={4} style={{ height: 250 }}>
          <StationPieChart data={pieData} />
        </Grid>
      </Grid>
    </Card>
  );
};

const useStyles = makeStyles((theme) => ({
  root: {
    // maxWidth: 300,
    maxWidth: "100%",
    height: "100%",
    backgroundColor: "#fff",
    padding: "15px 15px 25px 15px",
  },

  title: {
    fontSize: 18,
    color: "#939393",
    fontWeight: 800,
  },

  figure: {
    fontSize: 50,
    fontWeight: 800,
    color: "#E7843C",
    marginBottom: 40,
  },
  figureSmall: {
    fontSize: 25,
    fontWeight: 800,
    color: "#E7843C",
    marginBottom: 40,
  },
  updated: {
    marginTop: 12,
    fontWeight: 400,
    color: "#939393",
  },
  wrapIcon: {
    verticalAlign: "text-top",
    display: "inline-flex",
    fontWeight: 600,
  },
  spaceRight: {
    marginRight: theme.spacing(2),
  },
  spaceLeft: {
    marginLeft: theme.spacing(2),
  },
  spaceChip: {
    marginLeft: theme.spacing(1),
    color: "white",
    height: "80%",
  },
  positionListItem: {
    marginLeft: "-20px",
    marginTop: "-15px",
  },
}));

export default StationMainCard;
