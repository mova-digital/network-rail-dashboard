import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  Card,
  Menu,
  MenuItem,
  CardContent,
  Button,
  Typography,
  Divider,
  Grid,
  Chip,
} from "@material-ui/core";
import { Link } from "components";
import LinkedCameraIcon from "@material-ui/icons/LinkedCamera";
import MapIcon from "@material-ui/icons/Map";
import WebIcon from "@material-ui/icons/Web";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import PeopleAltIcon from "@material-ui/icons/PeopleAlt";
import MonetizationOnIcon from "@material-ui/icons/MonetizationOn";
import VisibilityIcon from "@material-ui/icons/Visibility";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Moment from "react-moment";
import "moment-timezone";

const StationSingleCard = ({ stationData, area, adv }) => {
  const classes = useStyles();

  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <Card
      key={stationData.id}
      className={classes.root}
      style={{ border: "none", boxShadow: "none" }}
    >
      <Typography color="textSecondary" align="right">
        <Button
          aria-controls="simple-menu"
          aria-haspopup="true"
          onClick={handleClick}
        >
          <MoreVertIcon />
        </Button>
        <Menu
          id="card-menu"
          anchorEl={anchorEl}
          keepMounted
          open={Boolean(anchorEl)}
          onClose={handleClose}
        >
          <MenuItem
            component={Link}
            href="/station/[id]"
            as={`/station/${stationData.id}`}
          >
            Detailed View
          </MenuItem>
          <MenuItem style={{ color: "black" }} component={Link} href="/compare">
            Compare
          </MenuItem>
        </Menu>
      </Typography>

      <CardContent style={{ padding: 10 }}>
        <Typography
          className={classes.title}
          color="textSecondary"
          gutterBottom
        >
          {/* {stationData.cityName.toUpperCase()} */}
        </Typography>
        {area ? (
          <Typography
            className={
              stationData.name.length >= 26
                ? classes.stationNameSmall
                : classes.stationNameLarge
            }
            color="textSecondary"
            gutterBottom
            variant="h3"
            component="p"
          >
            {stationData.name}
          </Typography>
        ) : (
          <Typography
            className={classes.stationNameLarge}
            color="textSecondary"
            variant="h3"
            component="p"
          >
            {stationData.name?.toUpperCase()}
          </Typography>
        )}
        <Typography variant="h6" className={classes.operatorSection}>
          Operator:
          <img
            src="../nr-logo.svg"
            alt="logo"
            className={classes.operatorLogo}
          />
        </Typography>
        <ListItem className={classes.positionListItem}>
          <MapIcon />
          <ListItemText className={classes.spaceLeft}>
            {stationData.zonesNum > 1
              ? stationData.zonesNum + " Zones"
              : stationData.zonesNum + " Zone"}
          </ListItemText>
        </ListItem>
        <ListItem className={classes.positionListItem}>
          <LinkedCameraIcon />{" "}
          <ListItemText className={classes.spaceLeft}>
            {stationData.camerasNum > 1
              ? stationData.camerasNum + " Cameras"
              : stationData.camerasNum === 0
              ? stationData.camerasNum + " Cameras"
              : stationData.camerasNum + " Camera"}
          </ListItemText>
        </ListItem>

        {adv ? (
          <>
            {" "}
            <Typography variant="body1">
              <Grid
                item
                style={{ padding: 17, paddingRight: "80%", paddingLeft: 0 }}
              >
                <Divider />
              </Grid>
            </Typography>
            <Grid container spacing={1}>
              <Grid item>
                <Typography variant="body2" className={classes.wrapIcon}>
                  <WebIcon className={classes.spaceRight} />{" "}
                  {stationData.advertisingZones > 1
                    ? stationData.advertisingZones + " Advertising Zones"
                    : stationData.advertisingZones + " Advertising Zone"}
                </Typography>{" "}
              </Grid>
              <Grid item xs={12}>
                <Typography variant="body2" className={classes.wrapIcon}>
                  <PeopleAltIcon className={classes.spaceRight} />{" "}
                  {stationData.peopleFootfall} people footfall{" "}
                  <Chip
                    // color={
                    //   Number(stationData.peopleFootfallTrend) < 0
                    //     ? "secondary"
                    //     : "primary"
                    // }
                    style={
                      Number(stationData.peopleFootfallTrend) < 0
                        ? { backgroundColor: "#50D050", color: "white" }
                        : { backgroundColor: "#ef5350", color: "white" }
                    }
                    className={classes.spaceChip}
                    label={
                      stationData.peopleFootfallTrend == undefined
                        ? ""
                        : stationData.peopleFootfallTrend > 0
                        ? "+" +
                          stationData.peopleFootfallTrend?.toFixed(2) +
                          "%"
                        : "" + stationData.peopleFootfallTrend?.toFixed(2) + "%"
                    }
                    size="small"
                  />
                </Typography>{" "}
              </Grid>
              <Grid item>
                <Typography variant="body2" className={classes.wrapIcon}>
                  <VisibilityIcon className={classes.spaceRight} /> £
                  {stationData.costPerView} Cost per View{" "}
                  <Chip
                    // color={
                    //   Number(stationData.costPerViewTrend) > 0
                    //     ? "secondary"
                    //     : "primary"
                    // }
                    style={
                      Number(stationData.costPerViewTrend) > 0
                        ? { backgroundColor: "#ef5350", color: "white" }
                        : { backgroundColor: "#50D050", color: "white" }
                    }
                    className={classes.spaceChip}
                    label={
                      stationData.costPerViewTrend == undefined
                        ? ""
                        : stationData.costPerViewTrend > 0
                        ? "+" + stationData.costPerViewTrend?.toFixed(2) + "%"
                        : "" + stationData.costPerViewTrend?.toFixed(2) + "%"
                    }
                    size="small"
                  />
                </Typography>{" "}
              </Grid>
              <Grid item>
                <Typography variant="body2" className={classes.wrapIcon}>
                  <MonetizationOnIcon className={classes.spaceRight} /> £
                  {stationData.benefitsRealised?.toFixed(2)} Benefits Realised{" "}
                  <Chip
                    // color={
                    //   Number(stationData.benefitsRealisedTrend) < 0
                    //     ? "secondary"
                    //     : "primary"
                    // }
                    style={
                      Number(stationData.benefitsRealisedTrend) < 0
                        ? { backgroundColor: "#ef5350", color: "white" }
                        : { backgroundColor: "#50D050", color: "white" }
                    }
                    className={classes.spaceChip}
                    label={
                      stationData.benefitsRealisedTrend == undefined
                        ? ""
                        : stationData.benefitsRealisedTrend >= 0.1
                        ? "+" +
                          stationData.benefitsRealisedTrend?.toFixed(2) +
                          "%"
                        : "" +
                          stationData.benefitsRealisedTrend?.toFixed(2) +
                          "%"
                    }
                    size="small"
                  />
                </Typography>{" "}
              </Grid>
            </Grid>
          </>
        ) : (
          ""
        )}

        {stationData.updatedAt == undefined ? (
          " "
        ) : (
          <Typography
            variant="body2"
            className={classes.updated}
            color="textSecondary"
          >
            {"Last updated: "}
            <Moment
              date={stationData?.updatedAt}
              format="ddd, MMMM Do YYYY"
              local
            ></Moment>{" "}
            (<Moment to={stationData.updatedAt} />)
          </Typography>
        )}
      </CardContent>
    </Card>
  );
};

const useStyles = makeStyles((theme) => ({
  root: {
    // width: 400,
    maxWidth: "100%",
    height: "100%",
    backgroundColor: "#fff",
    padding: "15px 15px",
  },

  title: {
    fontSize: 14,
    color: "#939393",
    fonteight: 800,
  },

  figure: {
    fontSize: 50,
    fontWeight: 800,
    color: "#E7843C",
    marginBottom: 40,
  },
  stationNameLarge: {
    fontSize: 25,
    fontWeight: 800,
    color: "#E7843C",
    marginBottom: 20,
  },
  stationNameSmall: {
    fontSize: 21,
    fontWeight: 800,
    color: "#E7843C",
    marginBottom: 20,
  },
  operatorSection: {
    display: "flex",
    justifyContent: "flex-start",
    alignItems: "center",
    margin: 15,
    marginLeft: -10,
    paddingLeft: 10,
    borderRadius: 15,
    backgroundColor: "#f8f8f8 ",
    width: "60%",
    height: 45,
    padding: 5,
  },
  operatorLogo: {
    height: "95%",
    marginLeft: 12,
  },
  updated: {
    marginTop: 16,
    fontWeight: 400,
    color: "#939393",
  },
  wrapIcon: {
    verticalAlign: "text-top",
    display: "inline-flex",
  },
  spaceRight: {
    marginRight: theme.spacing(2),
  },
  spaceLeft: {
    marginLeft: theme.spacing(2),
  },
  spaceChip: {
    marginLeft: theme.spacing(1),
    color: "white",
    height: "80%",
  },
  positionListItem: {
    marginLeft: "-20px",
    marginTop: "-15px",
  },
}));

export default StationSingleCard;
