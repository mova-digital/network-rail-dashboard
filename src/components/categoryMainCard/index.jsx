import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  Card,
  CardContent,
  Button,
  Typography,
  Grid,
  Divider,
} from "@material-ui/core";
import { LineAreaGradientChart } from "components";

import PictureAsPdfIcon from "@material-ui/icons/PictureAsPdf";

const CategoryMainCard = ({ categoriesData, valueUnitAbbrev, maxMin }) => {
  const classes = useStyles();

  return (
    <Card className={classes.root}>
      <CardContent className={classes.cardContent}>
        <Typography color="textSecondary"></Typography>
        <Button
          aria-controls="export-graph"
          aria-haspopup="true"
          onClick={() => console.log("Export Button clicked")}
        >
          <PictureAsPdfIcon />
        </Button>
      </CardContent>
      {categoriesData === "" ? (
        <Grid style={{ height: 450, margin: -15 }}></Grid>
      ) : (
        <Grid style={{ height: 450, margin: -15 }}>
          <LineAreaGradientChart
            data={categoriesData}
            maxMin={maxMin}
            valueUnitAbbrev={valueUnitAbbrev}
          />
        </Grid>
      )}
    </Card>
  );
};

const useStyles = makeStyles({
  root: {
    maxWidth: "100%",
    height: "100%",
    backgroundColor: "#fff",
    padding: "15px 15px",
    border: "none",
    boxShadow: "none",
  },
  cardContent: {
    padding: "0px 0px 5px 10px",
    display: "flex",
    justifyContent: "space-between",
  },
});

export default CategoryMainCard;
