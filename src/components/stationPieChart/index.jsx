// install (please make sure versions match peerDependencies)
// yarn add @nivo/core @nivo/pie
import React from "react";
import { ResponsivePie } from "@nivo/pie";
import { Typography } from "@material-ui/core";
// make sure parent container have a defined height when using
// responsive component, otherwise height will be 0 and
// no chart will be rendered.
// website examples showcase many properties,
// you'll often use just a few of them.
const CenteredMetric = ({ dataWithArc, centerX, centerY }) => {
  let values = [];
  dataWithArc.forEach((datum) => {
    values.push(datum.value);
  });
  console.log(values);
  return (
    <text
      x={centerX}
      y={centerY}
      textAnchor="middle"
      dominantBaseline="central"
      fill="hsl(25.3, 78.1%, 57.1%)"
      style={{
        fontSize: "42px",
        fontWeight: "400",
      }}
    >
      {Math.max(...values)}%
    </text>
  );
};
const StationPieChart = ({ data }) => (
  <div style={styles.root}>
    <ResponsivePie
      data={data}
      sortByValue={false}
      // colors={{ datum: "data.color" }}
      margin={{ top: 10, right: 10, bottom: 10, left: 10 }}
      innerRadius={0.8}
      activeOuterRadiusOffset={8}
      colors={{ scheme: "oranges" }}
      enableArcLinkLabels={false}
      arcLinkLabelsSkipAngle={10}
      arcLinkLabelsTextColor="#333333"
      arcLinkLabelsThickness={2}
      arcLinkLabelsColor={{ from: "color" }}
      arcLabelsSkipAngle={10}
      arcLabelsTextColor={{ from: "color", modifiers: [["darker", 2]] }}
      radialLabel={(d) => `${d.id} (${d.formattedValue})`}
      layers={["arcs", "arcLabels", "arcLinkLabels", "legends", CenteredMetric]}
      enableSliceLabels={false}
    />{" "}
    <Typography
      variant="h6"
      color="textSecondary"
      component="h3"
      gutterBottom
      style={styles.chartTitle}
    >
      FACEMASK COMPLIANCE
    </Typography>
  </div>
);

const margin = { top: 30, right: 200, bottom: 30, left: 30 };

const styles = {
  root: {
    textAlign: "center",
    position: "relative",
    width: "100%",
    height: "100%",
  },

  chartTitle: {
    fontWeight: 200,
    fontSize: 14,
    color: "#939393",
  },
};
export default StationPieChart;
