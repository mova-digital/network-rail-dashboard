import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  Card,
  Menu,
  MenuItem,
  CardContent,
  Button,
  Typography,
  Grid,
  Divider,
  Chip,
} from "@material-ui/core";
import { Link, GradientAreaLine } from "components";
import LinkedCameraIcon from "@material-ui/icons/LinkedCamera";
import MapIcon from "@material-ui/icons/Map";
import WebIcon from "@material-ui/icons/Web";
import PeopleAltIcon from "@material-ui/icons/PeopleAlt";
import MonetizationOnIcon from "@material-ui/icons/MonetizationOn";
import VisibilityIcon from "@material-ui/icons/Visibility";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import MoreVertIcon from "@material-ui/icons/MoreVert";

const dummyData = [
  {
    id: "waterloo",
    color: "hsl(38.2, 100%, 50%)",
    data: [
      {
        x: "2021-07-28T00:00:00.000Z",
        y: 104,
      },
      {
        x: "2021-07-29T00:00:00.000Z",
        y: 92,
      },
      {
        x: "2021-07-30T00:00:00.000Z",
        y: 104,
      },
      {
        x: "2021-07-31T00:00:00.000Z",
        y: 82,
      },
      {
        x: "2021-08-01T00:00:00.000Z",
        y: 58,
      },
      {
        x: "2021-08-02T00:00:00.000Z",
        y: 62,
      },
      {
        x: "2021-08-03T00:00:00.000Z",
        y: 119,
      },
    ],
  },
];

const DetailedStationCard = ({ name, data }) => {
  const classes = useStyles();

  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const cleanDate = (dateString) => {
    let d = new Date(dateString);
    return d.toUTCString();
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <Card
      className={classes.root}
      style={{ border: "none", boxShadow: "none" }}
    >
      <Typography color="textSecondary" align="right">
        <Button
          aria-controls="simple-menu"
          aria-haspopup="true"
          onClick={handleClick}
        >
          <MoreVertIcon />
        </Button>
        <Menu
          id="card-menu"
          anchorEl={anchorEl}
          keepMounted
          open={Boolean(anchorEl)}
          onClose={handleClose}
        >
          <MenuItem component={Link} href="/station/[id]" as={`/station/1`}>
            See More
          </MenuItem>
          <MenuItem onClick={handleClose}>Export PDF</MenuItem>
          <MenuItem onClick={handleClose}>Export XSL</MenuItem>
        </Menu>
      </Typography>
      <CardContent style={{ padding: 10 }}>
        <Typography
          className={classes.title}
          color="textSecondary"
          gutterBottom
        >
          {!name ? "Station name" : name.toUpperCase()}
        </Typography>
        <Typography variant="h6" className={classes.operatorSection}>
          Operator:
          <img
            src="../nr-logo.svg"
            alt="logo"
            className={classes.operatorLogo}
          />
        </Typography>
        <ListItem className={classes.positionListItem}>
          <LinkedCameraIcon />
          <ListItemText className={classes.spaceLeft}>
            {data[0]?.zonesNum} Zones
          </ListItemText>
        </ListItem>
        <ListItem className={classes.positionListItem}>
          <MapIcon />{" "}
          <ListItemText className={classes.spaceLeft}>
            {data[0]?.camerasNum} Cameras
          </ListItemText>
        </ListItem>
        <Typography variant="body1">
          <Grid
            item
            style={{ padding: 17, paddingRight: "80%", paddingLeft: 0 }}
          >
            <Divider />
          </Grid>
        </Typography>
        <ListItem className={classes.positionListItem}>
          <WebIcon className={classes.spaceRight} />
          <ListItemText className={classes.spaceLeft}>
            {data[0]?.advertisingZonesNum} Advertising Zones
          </ListItemText>
        </ListItem>
        <ListItem className={classes.positionListItem}>
          <PeopleAltIcon className={classes.spaceRight} />{" "}
          <ListItemText className={classes.spaceLeft}>
            {data[0]?.peopleFootfall} people footfall{" "}
            <Chip
              color={
                Number(data.peopleFootfallTrend) < 0 ? "secondary" : "primary"
              }
              className={classes.spaceChip}
              // label={data[0]?.peopleFootfallTrend?.toFixed(1) + "%"}
              label={"3.45%"}
              size="small"
            />
          </ListItemText>
        </ListItem>
        <ListItem className={classes.positionListItem}>
          <VisibilityIcon className={classes.spaceRight} />{" "}
          <ListItemText className={classes.spaceLeft}>
            £{data[0]?.costPerView} Cost per View{" "}
            <Chip
              color={
                Number(data[0]?.costPerViewTrend) < 0 ? "secondary" : "primary"
              }
              className={classes.spaceChip}
              label={
                data[0]?.costPerViewTrend != undefined
                  ? data[0]?.costPerViewTrend + "%"
                  : " "
              }
              size="small"
            />
          </ListItemText>
        </ListItem>{" "}
        <ListItem className={classes.positionListItem}>
          <MonetizationOnIcon className={classes.spaceRight} />{" "}
          <ListItemText className={classes.spaceLeft}>
            £{data[0]?.benefitsRealised.toFixed(2)} Benefits Realised{" "}
            <Chip
              color={
                Number(data[0]?.benefitsRealisedTrend) < 0
                  ? "secondary"
                  : "primary"
              }
              className={classes.spaceChip}
              // label={data[0]?.benefitsRealisedTrend + "%"}
              label={"+34.21%"}
              size="small"
            />
          </ListItemText>
        </ListItem>{" "}
        <Typography
          variant="body2"
          className={classes.updated}
          color="textSecondary"
        >
          Updated at: {cleanDate(data[0]?.updatedAt)}
        </Typography>
        <Typography
          variant="body2"
          className={classes.updated}
          color="textSecondary"
        >
          CUSTOMERS ONSITE (Hourly values){" "}
        </Typography>
      </CardContent>

      <Grid style={{ height: 250, margin: -15 }}>
        <GradientAreaLine graphData={dummyData} valueUnitAbbrev={"people"} />
      </Grid>
    </Card>
  );
};

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: "100%",
    backgroundColor: "#fff",
    padding: "15px 15px",
  },

  title: {
    fontSize: 20,
    color: "#939393",
    fontWeight: 800,
  },

  figure: {
    fontSize: 50,
    fontWeight: 800,
    color: "#E7843C",
    marginBottom: 40,
  },
  updated: {
    marginTop: 12,
    fontWeight: 400,
    color: "#939393",
  },
  stationName: {
    fontSize: 25,
    fontWeight: 800,
    color: "#E7843C",
    marginBottom: 20,
  },
  operatorSection: {
    display: "flex",
    justifyContent: "flex-start",
    alignItems: "center",
    margin: 15,
    marginLeft: -10,
    paddingLeft: 10,
    borderRadius: 15,
    backgroundColor: "#f8f8f8 ",
    width: "40%",
    height: 45,
    padding: 5,
  },
  operatorLogo: {
    height: "95%",
    marginLeft: 12,
  },
  wrapIcon: {
    verticalAlign: "text-top",
    display: "inline-flex",
  },
  spaceRight: {
    marginRight: theme.spacing(2),
  },
  spaceLeft: {
    marginLeft: theme.spacing(2),
  },
  spaceChip: {
    marginLeft: theme.spacing(1),
    color: "white",
    height: "80%",
  },
  positionListItem: {
    marginLeft: "-20px",
    marginTop: "-15px",
  },
}));

export default DetailedStationCard;
