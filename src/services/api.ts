import axios from "axios";
import { parseCookies } from "nookies";

const api = axios.create({
  baseURL: "https://sabre.mova.ie", //TODO added env var
  timeout: 3000,
});

const createAuthorizationHeader = async () => {
  const { "nextauth.token": token } = parseCookies();
  return {
    Authorization: `Bearer ${token}`,
  };
};

/**
 * This is the entity for the index page to give an overview of each station.
 * The return data will cover the overview data for each station for number of devices, ads etc.
 * @returns {object} Returns overview data all stations.
 */
export const StationsListGeneralData = async () => {
  const allStations = [];
  try {
    const response = await api.post(
      "data-fetch/allStations",
      { allStations },
      {
        headers: await createAuthorizationHeader(),
      }
    );
    return response.data;
  } catch (error) {
    console.error(error.response.status);
  }
};

/**
 * This is the entity for when a station has been selected for detailed view.
 * The return data will cover the overview data for each area in the station for number of devices, ads etc.
 * @param {string} stationId - Station id.
 * @returns {object} Returns overview data for each area in the station.
 */
export const StationDetails = async (stationId: string) => {
  const allStations = [{ stationId }];
  try {
    const response = await api.post(
      "data-fetch/allStations",
      { allStations },
      {
        headers: await createAuthorizationHeader(),
      }
    );
    return response.data;
  } catch (error) {
    console.error(error.response.status);
  }
};

/**
 * This is the entity for the index page that will return overview data for the graphs etc.
 * Averaging across all stations.
 * @returns {object} Returns average data all stations.
 */
export const AverageChartAllStations = async () => {
  const index = [];
  try {
    const response = await api.post(
      "data-fetch/index",
      { index },
      {
        headers: await createAuthorizationHeader(),
      }
    );
    return response.data;
  } catch (error) {
    console.error(error.response.status);
  }
};

/**
 * This is the entity for when a station has been selected for detail.
 * The return data will cover the overview data for the graphs etc. for the station.
 * @param {string} stationId - Station id.
 * @returns {object} Returns average data from corresponding station.
 */
export const AverageChartOneStation = async (stationId: string) => {
  const index = [{ stationId }];
  try {
    const response = await api.post(
      "data-fetch/index",
      { index },
      {
        headers: await createAuthorizationHeader(),
      }
    );
    return response.data;
  } catch (error) {
    console.error(error.response.status);
  }
};

/**
 * This is the entity for when an area has been selected for detail.
 * The return data will cover the overview data for the graphs etc. for the area.
 * @param {string} areaId - Area id.
 * @returns {object} Returns data from corresponding area.
 */
export const AverageChartOneArea = async (areaId: string) => {
  const index = [{ areaId }];
  try {
    const response = await api.post(
      "data-fetch/index",
      { index },
      {
        headers: await createAuthorizationHeader(),
      }
    );
    return response.data;
  } catch (error) {
    console.error(error.response.status);
  }
};

/**
 * This is the entity for searching for a specific datapoint, in a given timeframe, for stations with a given name value.
 * @param {object[]} filters - Check the filter format in the api documentation.
 * @returns {object} Returns data from corresponding stations.
 */
export const SearchByStation = async (filters: object[]) => {
  const stations = [...filters];
  try {
    const response = await api.post(
      "data-fetch/stations",
      { stations },
      {
        headers: await createAuthorizationHeader(),
      }
    );
    return response.data;
  } catch (error) {
    console.error(error.response.status);
  }
};

/**
 * This is the entity for all the areas in a station when filtering for a specific datapoint.
 * @param {object[]} filters - Check the filter format in the api documentation.
 * @returns {object} Returns data from corresponding areas.
 */
export const SearchForAreasOfStation = async (filters: object[]) => {
  const stations = { areas: [...filters] };
  try {
    const response = await api.post(
      "data-fetch/stations",
      { stations },
      {
        headers: await createAuthorizationHeader(),
      }
    );
    return response.data;
  } catch (error) {
    console.error(error.response.status);
  }
};

export default api;
