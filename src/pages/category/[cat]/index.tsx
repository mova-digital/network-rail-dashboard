import React from "react";
import axios from "axios";
import Head from "next/head";
import { useRouter } from "next/router";
import { GetServerSideProps, NextPage } from "next";
import ErrorPage from "next/error";
import {
  Container,
  Grid,
  Typography,
  Chip,
  makeStyles,
  FormControl,
  Select,
  MenuItem,
  Button,
  Input,
  ListItemText,
  Checkbox,
} from "@material-ui/core";
import {
  NavigationBar,
  OptionsBar,
  StationSingleCard,
  CategoryMainCard,
  InsightsCard,
} from "components";

import FavoriteBorderIcon from "@material-ui/icons/FavoriteBorder";
import FavoriteIcon from "@material-ui/icons/Favorite";

import {
  OptionsContext,
  OptionsProvider,
} from "../../../components/optionsBar/provider";

import MuiAlert, { AlertProps } from "@material-ui/lab/Alert";

function Alert(props: AlertProps) {
  return <MuiAlert elevation={1} variant="standard" {...props} />;
}

type Data = {
  id: string;
  color: string;
  data: Array<string>;
};

const CategoryPage: NextPage<{
  result0Data: Data;
  result1Data: Data;
  result2Data: Data;
  result3Data: Data;
}> = (props) => {
  const classes = useStyles();
  const router = useRouter();
  const { cat } = router.query;
  const options = React.useContext(OptionsContext);
  console.log("options.category: " + options.category);

  console.log("cat at index [cat]: " + cat);

  const categoryNames = [
    "Average Dwell Time",
    "Advertising Trend",
    "Facemask Compliance",
    "Customers Onsite",
  ];

  const stationCategoryData = [
    {
      id: "980d84a4-77b6-47ed-9b86-5c22f2720c39",
      name: "Chadwell Heath",
      camerasNum: 8,
      zonesNum: 9,
      advertisingZones: 9,
      costPerView: 1.3,
      benefitsRealised: 10240.7,
      peopleFootfall: 11339,
      updatedAt: "2021-06-21T23:59:59.999Z",
    },
    {
      id: "980d84a4-77b6-47ed-9b86-5c22f2720c39",
      name: "Waterloo",
      camerasNum: 18,
      zonesNum: 17,
      advertisingZones: 19,
      costPerView: 1.9,
      benefitsRealised: 10240.7,
      peopleFootfall: 11339,
      updatedAt: "2021-06-21T23:59:59.999Z",
    },
    {
      id: "980d84a4-77b6-47ed-9b86-5c22f2720c39",
      name: "Gidea Park",
      camerasNum: 51,
      zonesNum: 9,
      advertisingZones: 19,
      costPerView: 1.6,
      benefitsRealised: 15240.7,
      peopleFootfall: 13339,
      updatedAt: "2021-06-21T23:59:59.999Z",
    },
  ];

  const categorySpecificData = {
    name: "Advertising Trend",
    avgValue: 2696.2133004926127,
    valueUnit: "£",
    measureUnitAbbrev: " ",
    avgValTrendPercentage: 38.925096816850846,
    avgValTrend: 412.0999999999999,
    graphData: [
      {
        id: "Advertising Trend",
        color: "hsl(38.2, 100%, 50%)",
        data: [
          {
            x: "Monday",
            y: 1239.4,
          },
          {
            x: "Tuesday",
            y: 1777.6,
          },
          {
            x: "Wednesday",
            y: 1058.7,
          },
          {
            x: "Thursday",
            y: 1239.4,
          },
          {
            x: "Friday",
            y: 3259.6,
          },
          {
            x: "Saturday",
            y: 1058.7,
          },
          {
            x: "Sunday",
            y: 1470.8,
          },
        ],
      },
    ],
  };
  return (
    <>
      <NavigationBar />
      <Container maxWidth="lg" style={{ marginTop: "80px" }}>
        <Head>
          <title>Network Rail Homepage</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>

        <Grid container className={classes.root}>
          <Grid container alignItems="center">
            <Grid item xs={12} className={classes.demoAlert}>
              <Alert severity="warning">
                <strong>This page contains demo data.</strong>
              </Alert>
            </Grid>
            <Grid item>
              <Typography variant="h6" gutterBottom>
                Compare category:{" "}
              </Typography>
            </Grid>
            <Grid item>
              <FormControl
                variant="outlined"
                className={classes.formControlCategory}
              >
                <Select
                  style={{ backgroundColor: "white" }}
                  className={classes.formControlCategory}
                  labelId="select-category"
                  id="select-category"
                  displayEmpty
                  value={options.category}
                  renderValue={() =>
                    options.category.length === 0 ? (
                      <em>Select a category</em>
                    ) : (
                      options.category
                    )
                  }
                  onChange={options.handleCategoryChange}
                >
                  <MenuItem disabled>
                    <em>Please select a category</em>
                  </MenuItem>
                  {categoryNames.map((name) => (
                    <MenuItem key={name} value={name}>
                      <Checkbox
                        icon={<FavoriteBorderIcon />}
                        checkedIcon={<FavoriteIcon />}
                        checked={options.stations.indexOf(name) > -1}
                      />
                      <ListItemText primary={name} />
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Grid>
          </Grid>

          <OptionsBar initialCat={cat.toString()} station={false} ctg={true} />
          <Grid item xs={12} style={{ padding: 0, marginTop: 15 }}>
            <Typography variant="h6" gutterBottom>
              Category:{" "}
              {!options.category && cat === "" ? (
                <Typography
                  variant="body1"
                  display="inline"
                  style={{ fontStyle: "italic" }}
                >
                  No category selected
                </Typography>
              ) : !options.category && cat !== "" ? (
                <Typography variant="h6" display="inline">
                  {<Chip label={cat} onDelete={() => ""} />} {"   "}
                </Typography>
              ) : (
                <Typography variant="h6" display="inline">
                  {
                    <Chip
                      label={options.category === "" ? cat : options.category}
                      onDelete={() =>
                        options.handleStationChipRemove(options.category)
                      }
                    />
                  }{" "}
                  {"   "}
                </Typography>
              )}
              <Typography variant="h6" display="inline">
                Stations:{" "}
                {options.stations.length === 0 ? (
                  <Typography
                    variant="body1"
                    display="inline"
                    style={{ fontStyle: "italic" }}
                  >
                    No stations selected
                  </Typography>
                ) : (
                  (options.stations as string[]).map((value) => (
                    <>
                      <Chip
                        key={value}
                        label={value}
                        onDelete={() => options.handleStationChipRemove(value)}
                      />{" "}
                    </>
                  ))
                )}
              </Typography>
              {"   "}
              <Typography variant="h6" display="inline">
                Areas:{" "}
                {options.areas.length === 0 ? (
                  <Typography
                    variant="body1"
                    display="inline"
                    style={{ fontStyle: "italic" }}
                  >
                    Areas not selected
                  </Typography>
                ) : options.stations.length >= 2 ? (
                  <Typography
                    variant="body1"
                    display="inline"
                    style={{ fontStyle: "italic" }}
                  >
                    Areas not selectable
                  </Typography>
                ) : (
                  (options.areas as string[]).map((value) => (
                    <>
                      <Chip
                        key={value}
                        label={value}
                        onDelete={() => options.handleAreaChipRemove(value)}
                      />{" "}
                    </>
                  ))
                )}
              </Typography>
            </Typography>
            <Grid item xs={12}>
              <CategoryMainCard
                categoriesData={
                  options.category === "Advertising Trend"
                    ? props.result0Data
                    : options.category === "Customers Onsite"
                    ? props.result1Data
                    : options.category === "Average Dwell Time"
                    ? props.result2Data
                    : options.category === "Facemask Compliance"
                    ? props.result3Data
                    : ""
                }
                valueUnitAbbrev={
                  options.category === "Advertising Trend"
                    ? "£"
                    : options.category === "Customers Onsite"
                    ? "people"
                    : options.category === "Average Dwell Time"
                    ? "seconds"
                    : options.category === "Facemask Compliance"
                    ? "%"
                    : ""
                }
                maxMin={false}
              />
            </Grid>
            <Grid container spacing={2} style={{ marginTop: 15 }}>
              {stationCategoryData.map((val) => (
                <Grid item xs={4}>
                  <Grid container spacing={2}>
                    <Grid item xs={12}>
                      <StationSingleCard
                        stationData={val}
                        area={false}
                        adv={true}
                      />
                    </Grid>
                    <Grid item xs={12}>
                      <InsightsCard data={categorySpecificData} />
                    </Grid>
                  </Grid>
                </Grid>
              ))}
            </Grid>
          </Grid>
        </Grid>
      </Container>
    </>
  );
};

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    marginTop: "2%",
  },
  demoAlert: {
    marginBottom: "1%",
  },
  formControlCategory: {
    marginLeft: 5,
    width: "100%",
    height: "100%",
  },
}));

export const getServerSideProps: GetServerSideProps = async ({
  params,
  res,
}) => {
  try {
    const { cat } = params;

    const result0 = await axios.get(`http://localhost:3003/api/category0`);
    const result1 = await axios.get(`http://localhost:3003/api/category1`);
    const result2 = await axios.get(`http://localhost:3003/api/category2`);
    const result3 = await axios.get(`http://localhost:3003/api/category3`);

    const result0Data: Data = await result0.data;
    const result1Data: Data = await result1.data;
    const result2Data: Data = await result2.data;
    const result3Data: Data = await result3.data;

    return {
      props: { result0Data, result1Data, result2Data, result3Data },
    };
  } catch {
    res.statusCode = 404;
    return {
      props: {},
    };
  }
};

export default CategoryPage;
