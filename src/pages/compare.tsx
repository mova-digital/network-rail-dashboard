import React, { useContext } from "react";
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
import { Container, Typography, Grid } from "@material-ui/core";
import axios from "axios";
import Head from "next/head";
import {
  OptionsBar,
  NavigationBar,
  InsightsCard,
  DetailedCompareStationCard,
} from "components";
import { OptionsContext } from "../components/optionsBar/provider";
import { GetServerSideProps, NextPage } from "next";
import {
  StationsListGeneralData,
  AverageChartAllStations,
} from "../hooks/useSWR";
import { AuthContext } from "../contexts/AuthContext";
import MuiAlert, { AlertProps } from "@material-ui/lab/Alert";

type SingleCategoryData = {
  id: string;
  name: string;
  avgValue: string;
  createdAt: string;
  data: Array<string>;
};

type CompareStationData = {
  id: string;
  name: string;
  avgValue: string;
  createdAt: string;
  data: Array<string>;
};

function Alert(props: AlertProps) {
  return <MuiAlert elevation={1} variant="standard" {...props} />;
}

const CompareIndexPage: NextPage<{
  // categories: SingleCategoryData[];
  // stations: CompareStationData[];
  firstStationData: CompareStationData[];
  secondStationData: CompareStationData[];
}> = (props) => {
  const { isAuthenticated, user, checkTokenUpdateNecessary } =
    useContext(AuthContext);
  let categories = [],
    stations = [];
  const classes = useStyles();
  if (isAuthenticated) {
    checkTokenUpdateNecessary();
    const stationsData = StationsListGeneralData();
    const categoriesData = AverageChartAllStations();
    if (stationsData.data) stations = stationsData.data;
    if (categoriesData.data) categories = categoriesData.data;
  } else {
    console.log("Not logged in!!");
  }

  const options = React.useContext(OptionsContext);

  return (
    <>
      <NavigationBar />
      <Container maxWidth="lg" style={{ marginTop: "80px" }}>
        <div>
          <Alert severity="warning">
            <strong>This page contains demo data.</strong>
          </Alert>
        </div>
        <Head>
          <title>Network Rail Homepage</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>

        <OptionsBar initialCat={""} station={false} ctg={false} />
        <Grid container className={classes.root}>
          <Grid item xs={12} style={{ padding: 0 }}>
            <Typography variant="h6" gutterBottom>
              {!options.firstLocation && !options.secondLocation
                ? "Please select two stations."
                : `${options.firstLocation} vs ${options.secondLocation}`}
            </Typography>
            <Grid container spacing={3}>
              <Grid item xs={12} sm={8} md={6} lg={6}>
                <DetailedCompareStationCard
                  name={options.firstLocation}
                  data={stations}
                />
              </Grid>
              <Grid item xs={12} md={6} lg={6}>
                <DetailedCompareStationCard
                  name={options.secondLocation}
                  data={props.secondStationData}
                />
              </Grid>
            </Grid>
          </Grid>
          <Grid item xs={12} style={{ padding: 0, marginTop: 15 }}>
            <Typography variant="h6" gutterBottom>
              Overview
            </Typography>
            <Grid container spacing={1}>
              {categories.map((cat) => (
                <Grid key={cat.id} item xs={6} sm={4} md={3}>
                  <InsightsCard data={cat} />
                </Grid>
              ))}
            </Grid>
          </Grid>
        </Grid>
      </Container>
    </>
  );
};

export const getServerSideProps: GetServerSideProps = async () => {
  // const categories: SingleCategoryData = await GetCategories();
  // const stations: CompareStationData = await GetStations();

  const firstStationDataResult = await axios.get(
    `http://localhost:3003/api/firstStation`
  );
  const secondStationDataResult = await axios.get(
    `http://localhost:3003/api/secondStation`
  );

  const firstStationData: CompareStationData =
    await firstStationDataResult.data;
  const secondStationData: CompareStationData =
    await secondStationDataResult.data;

  return {
    props: { firstStationData, secondStationData },
    // props: { categories, stations, firstStationData, secondStationData },
  };
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
      marginTop: "2%",
    },
    paper: {
      height: 140,
      width: 100,
    },
    control: {
      padding: theme.spacing(2),
    },
  })
);

export default CompareIndexPage;
