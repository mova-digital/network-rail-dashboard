import React, { useContext, useEffect } from "react";
import { useRouter } from "next/router";
import { AuthContext } from "../../contexts/AuthContext";
import { RestricedAreaCard } from "src/components";

const Callback = () => {
  const router = useRouter();
  const { signIn } = useContext(AuthContext);
  const { code, state } = router.query;

  useEffect(() => {
    if (code && state) signIn({ code, state });
  }, [code, state]);

  return <RestricedAreaCard loading={true} />;
};

export default Callback;
