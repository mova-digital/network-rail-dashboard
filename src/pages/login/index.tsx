import React from "react";
import { RestricedAreaCard } from "src/components";

const LoginPage = () => {
  return (
    <>
      <RestricedAreaCard loading={false} />
    </>
  );
};

export default LoginPage;
