import React, { useContext } from "react";
import Head from "next/head";
import { Container, Grid, Typography, makeStyles } from "@material-ui/core";
import { useRouter } from "next/router";
import {
  NavigationBar,
  OptionsBar,
  StationSingleCard,
  StationSecondaryCard,
} from "components";
import { NextPage } from "next";
import { OptionsContext } from "../../../components/optionsBar/provider";
import {
  StationsListGeneralData,
  StationDetails,
  AverageChartOneStation,
} from "../../../hooks/useSWR";
import { AuthContext } from "../../../contexts/AuthContext";

type SingleAreaData = {
  id: string;
  name: string;
  avgValue: string;
  camerasNum: number;
  createdAt: string;
  data: Array<string>;
};

type SingleCategoryData = {
  id: string;
  name: string;
  avgValue: string;
  createdAt: string;
  data: Array<string>;
};

const SingleStationPage: NextPage<{
  areas: SingleAreaData[];
  stations: SingleAreaData[];
  categories: SingleCategoryData[];
}> = (props) => {
  const { isAuthenticated, user, checkTokenUpdateNecessary } =
    useContext(AuthContext);
  const classes = useStyles();

  const router = useRouter();
  const { id } = router.query;
  if (!id) {
    return <></>;
  }
  let categories = [],
    stations = [],
    areas = [];
  if (isAuthenticated) {
    checkTokenUpdateNecessary();
    const stationsData = StationsListGeneralData();
    const categoriesData = AverageChartOneStation(id);
    const areasData = StationDetails(id);
    if (stationsData.data) stations = stationsData.data;
    if (categoriesData.data) categories = categoriesData.data;
    if (areasData.data) areas = areasData.data;
  } else {
    console.log("Not logged in!!");
  }
  const station = stations?.filter((station) => station.id === id);
  const camAreas = areas?.filter((area) => area.camerasNum > 0);
  const noCamAreas = areas?.filter((area) => area.camerasNum <= 0);
  const sortedCategories = categories.sort(function (a, b) {
    var nameA = a?.name?.toUpperCase(); // ignore upper and lowercase
    var nameB = b?.name?.toUpperCase(); // ignore upper and lowercase
    if (nameA < nameB) {
      return -1;
    }
    if (nameA > nameB) {
      return 1;
    }

    // names must be equal
    return 0;
  });

  return (
    <>
      <NavigationBar />
      <Container maxWidth="lg" style={{ marginTop: "80px" }}>
        <Head>
          <title>Network Rail Homepage</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>
        <Grid container className={classes.root}>
          <Grid item xs={12} style={{ padding: 0, marginTop: 15 }}>
            <Grid container className={classes.stationNameRow}></Grid>
            <Grid container spacing={1}>
              <Grid item xs={4}>
                <StationSingleCard
                  stationData={station[0]}
                  area={false}
                  adv={true}
                />
              </Grid>

              {sortedCategories?.map((category) => (
                <Grid
                  key={category.name}
                  item
                  xs={category.name === "Advertising Trend" ? 8 : 4}
                >
                  <StationSecondaryCard stationCategoryData={category} />
                </Grid>
              ))}
            </Grid>

            <Grid item xs={12} style={{ padding: 0 }}>
              <div className={classes.pageSection}>
                <Typography variant="h6" gutterBottom style={{ marginTop: 10 }}>
                  Advertising Areas ({camAreas.length}):
                </Typography>
              </div>
            </Grid>

            {/* render advertising areas using single station component */}
            <Grid container spacing={1}>
              {camAreas?.map((area) => (
                <Grid key={area.id} item lg={4} md={6} sm={6}>
                  <StationSingleCard
                    area={true}
                    stationData={area}
                    adv={true}
                  />
                </Grid>
              ))}
            </Grid>
            <Grid item xs={12} style={{ padding: 0 }}>
              <div className={classes.pageSection}>
                <Typography variant="h6" gutterBottom style={{ marginTop: 10 }}>
                  Non-advertising Areas ({camAreas.length}):
                </Typography>
              </div>
            </Grid>
            <Grid container spacing={1}>
              {/* render non-advertising areas using single station component */}
              {noCamAreas?.map((area) => (
                <Grid key={area.id} item lg={4} md={4} sm={4}>
                  <StationSingleCard
                    area={true}
                    stationData={area}
                    adv={false}
                  />
                </Grid>
              ))}
            </Grid>
          </Grid>
        </Grid>
      </Container>
    </>
  );
};

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    marginTop: "2%",
  },
  optionBar: {
    height: 30,
    backgroundColor: "#aa760e",
  },
  paper: {
    height: 140,
    width: 100,
  },
  control: {
    padding: theme.spacing(2),
  },
  stationNameRow: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    margin: "10px 0px 10px 0px",
  },
  pageSection: {
    display: "flex",
    justifyContent: "space-between",
    marginTop: 25,
    marginBottom: 25,
  },
}));

export default SingleStationPage;
