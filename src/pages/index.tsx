import React, { useContext } from "react";
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
import { Container, Typography, Button, Grid } from "@material-ui/core";
import { GetServerSideProps, NextPage } from "next";
import Router from "next/router";
import { parseCookies } from "nookies";
import LaunchIcon from "@material-ui/icons/Launch";
import Head from "next/head";
import {
  StationsListGeneralData,
  AverageChartAllStations,
} from "../hooks/useSWR";
import { AuthContext } from "../contexts/AuthContext";
import {
  NavigationBar,
  InsightsCard,
  Link,
  StationSingleCard,
} from "components";

const IndexPage: NextPage<{}> = () => {
  const classes = useStyles();
  const { isAuthenticated, user, checkTokenUpdateNecessary } = useContext(
    AuthContext
  );
  let categories = [],
    stations = [];
  if (isAuthenticated) {
    checkTokenUpdateNecessary();
    const stationsData = StationsListGeneralData();
    const categoriesData = AverageChartAllStations();
    if (stationsData.data) stations = stationsData.data;
    if (categoriesData.data) categories = categoriesData.data;
  } else {
    console.log("Not logged in!!");
  }
  //console debugging
  console.log("user", user);
  console.log("categories", categories);

  return (
    <>
      <NavigationBar />
      <Container maxWidth="lg" style={{ marginTop: "80px" }}>
        <Head>
          <title>Network Rail Homepage</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>
        <Grid container className={classes.root}>
          <Grid item xs={12} style={{ padding: 0 }}>
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
                marginTop: 25,
                marginBottom: 25,
              }}
            >
              <Typography variant="h6" gutterBottom style={{ marginTop: 10 }}>
                Overview:
              </Typography>
              <Button
                color="primary"
                disableElevation
                style={{ color: "white" }}
                href="/category/compare"
                component={Link}
                variant="contained"
              >
                {"Reporting Tool"}
                <LaunchIcon style={{ marginLeft: 5 }} />
              </Button>{" "}
            </div>
            <Grid container spacing={1}>
              {categories.map((category) => (
                <Grid item lg={3} md={6} sm={6}>
                  <InsightsCard data={category} />
                </Grid>
              ))}
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={12} style={{ padding: 0 }}>
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              marginTop: 25,
              marginBottom: 25,
            }}
          >
            <Typography variant="h6" gutterBottom style={{ marginTop: 10 }}>
              All Stations ({stations.length}):
            </Typography>
            <Button
              color="primary"
              disableElevation
              style={{ color: "white" }}
              href="/compare"
              component={Link}
              variant="contained"
            >
              {"Compare two stations"}
              <LaunchIcon style={{ marginLeft: 5 }} />
            </Button>{" "}
          </div>
          <Grid container spacing={1}>
            {stations.map((station) => (
              <Grid item lg={4} md={6} sm={6} key={station.id}>
                <StationSingleCard
                  stationData={station}
                  area={false}
                  adv={true}
                />
              </Grid>
            ))}
          </Grid>
        </Grid>
      </Container>
    </>
  );
};

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const { ["nextauth.token"]: token } = parseCookies(ctx);

  if (!token) {
    return {
      redirect: {
        destination: "/login",
        permanent: false,
      },
    };
  }

  return {
    props: {},
  };
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
      marginTop: "2%",
    },
    optionBar: {
      height: 30,
      backgroundColor: "#aa760e",
    },
    paper: {
      height: 140,
      width: 100,
    },
    control: {
      padding: theme.spacing(2),
    },
  })
);

export default IndexPage;
