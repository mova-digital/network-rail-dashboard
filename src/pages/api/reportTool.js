// 20210627233155
// https://6098ed9299011f001713fadd.mockapi.io/api/areaLine/v1/category

export default (req, res) => {
  res.status(200).json([
    {
      id: "Chadwell Heath",
      color: "hsl(196, 70%, 50%)",
      data: [
        {
          x: "Monday",
          y: 131,
        },
        {
          x: "Tuesday",
          y: 185,
        },
        {
          x: "Wednesday",
          y: 223,
        },
        {
          x: "Thursday",
          y: 199,
        },
        {
          x: "Friday",
          y: 74,
        },
        {
          x: "Saturday",
          y: 57,
        },
        {
          x: "Sunday",
          y: 204,
        },
      ],
    },
    {
      id: "Gidea Park",
      color: "hsl(122, 70%, 50%)",
      data: [
        {
          x: "Monday",
          y: 55,
        },
        {
          x: "Tuesday",
          y: 74,
        },
        {
          x: "Wednesday",
          y: 211,
        },
        {
          x: "Thursday",
          y: 1,
        },
        {
          x: "Friday",
          y: 282,
        },
        {
          x: "Saturday",
          y: 199,
        },
        {
          x: "Sunday",
          y: 208,
        },
      ],
    },
    {
      id: "Waterloo",
      color: "hsl(174, 70%, 50%)",
      data: [
        {
          x: "Monday",
          y: 55,
        },
        {
          x: "Tuesday",
          y: 51,
        },
        {
          x: "Wednesday",
          y: 59,
        },
        {
          x: "Thursday",
          y: 147,
        },
        {
          x: "Friday",
          y: 0,
        },
        {
          x: "Saturday",
          y: 212,
        },
        {
          x: "Sunday",
          y: 192,
        },
      ],
    },
  ]);
};
