export default (req, res) => {
  res.status(200).json([
    {
      name: "Average Dwell Time",
      avgValue: 8.745543512830915,
      valueUnit: "seconds",
      measureUnitAbbrev: "%",
      avgValTrendPercentage: 54.59514668197439,
      avgValTrend: 7.806233240984197,
      graphData: [
        {
          id: "Average Dwell Time",
          color: "hsl(38.2, 100%, 50%)",
          data: [
            {
              x: "Monday",
              y: 1.1714736842105264,
            },
            {
              x: "Tuesday",
              y: 22.306774421801652,
            },
            {
              x: "Wednesday",
              y: 14.298401443001472,
            },
            {
              x: "Thursday",
              y: 1.1714736842105264,
            },
            {
              x: "Friday",
              y: 5.0369242424242415,
            },
            {
              x: "Saturday",
              y: 14.298401443001472,
            },
            {
              x: "Sunday",
              y: 22.10463468398567,
            },
          ],
        },
      ],
    },
    {
      name: "Advertising Trend",
      avgValue: 2696.2133004926127,
      valueUnit: "£",
      measureUnitAbbrev: "%",
      avgValTrendPercentage: 38.925096816850846,
      avgValTrend: 412.0999999999999,
      graphData: [
        {
          id: "Advertising Trend",
          color: "hsl(38.2, 100%, 50%)",
          data: [
            {
              x: "Monday",
              y: 1239.4,
            },
            {
              x: "Tuesday",
              y: 1777.6,
            },
            {
              x: "Wednesday",
              y: 1058.7,
            },
            {
              x: "Thursday",
              y: 1239.4,
            },
            {
              x: "Friday",
              y: 3259.6,
            },
            {
              x: "Saturday",
              y: 1058.7,
            },
            {
              x: "Sunday",
              y: 1470.8,
            },
          ],
        },
      ],
    },
    {
      name: "Facemask Compliance",
      avgValue: 47.28081769528679,
      valueUnit: "%",
      measureUnitAbbrev: "%",
      avgValTrendPercentage: 18.271090188106967,
      avgValTrend: 8.44449964746019,
      graphData: [
        {
          id: "Facemask Compliance",
          color: "hsl(38.2, 100%, 50%)",
          data: [
            {
              x: "Monday",
              y: 44.479721130199295,
            },
            {
              x: "Tuesday",
              y: 33.70258272800645,
            },
            {
              x: "Wednesday",
              y: 39.598330241187384,
            },
            {
              x: "Thursday",
              y: 51.234784350786974,
            },
            {
              x: "Friday",
              y: 46.217820395615426,
            },
            {
              x: "Saturday",
              y: 46.217820395615426,
            },
            {
              x: "Sunday",
              y: 54.66232004307562,
            },
          ],
        },
      ],
    },
    {
      name: "Customers Onsite",
      avgValue: 2458.6256157635466,
      valueUnit: "%",
      measureUnitAbbrev: "%",
      avgValTrendPercentage: 26.438698915763137,
      avgValTrend: 317,
      graphData: [
        {
          id: "Customers Onsite",
          color: "hsl(38.2, 100%, 50%)",
          data: [
            {
              x: "Monday",
              y: 1338,
            },
            {
              x: "Tuesday",
              y: 1752,
            },
            {
              x: "Wednesday",
              y: 1199,
            },
            {
              x: "Thursday",
              y: 1338,
            },
            {
              x: "Friday",
              y: 2892,
            },
            {
              x: "Saturday",
              y: 1199,
            },
            {
              x: "Sunday",
              y: 1516,
            },
          ],
        },
      ],
    },
  ]);
};
