export default (req, res) => {
  res.status(200).json([
    {
      id: "980d84a4-77b6-47ed-9b86-5c22f2720c39",
      cityName: "London",
      stationName: "Gidea Park",
      camerasNum: 8,
      zonesNum: 9,
      advertisingZonesNum: 9,
      costPerView: 1.3,
      costPerViewTrend: "+0.1",
      benefitsRealised: 10240.7,
      benefitsRealisedTrend: "+21",
      peopleFootfall: 11339,
      peopleFootfallTrend: "+13",
      updatedAt: "2021-06-21T23:59:59.999Z",
    },
  ]);
};
