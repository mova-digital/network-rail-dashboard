export default (req, res) => {
  res.status(200).json([
    {
      id: "Chadwell Heath",
      color: "hsl(196, 70%, 50%)",
      data: [
        { x: "09:00", y: 151 },
        { x: "10:00", y: 135 },
        { x: "11:00", y: 213 },
        { x: "12:00", y: 139 },
        { x: "13:00", y: 174 },
        { x: "14:00", y: 157 },
        { x: "15:00", y: 104 },
        { x: "16:00", y: 206 },
        { x: "17:00", y: 227 },
        { x: "18:00", y: 192 },
        { x: "19:00", y: 202 },
        { x: "20:00", y: 84 },
      ],
    },
    {
      id: "Waterloo",
      color: "hsl(122, 70%, 50%)",
      data: [
        { x: "09:00", y: 155 },
        { x: "10:00", y: 14 },
        { x: "11:00", y: 111 },
        { x: "12:00", y: 211 },
        { x: "13:00", y: 182 },
        { x: "14:00", y: 99 },
        { x: "15:00", y: 28 },
        { x: "16:00", y: 88 },
        { x: "17:00", y: 114 },
        { x: "18:00", y: 89 },
        { x: "19:00", y: 11 },
        { x: "20:00", y: 40 },
      ],
    },
    {
      id: "Gidea Park",
      color: "hsl(174, 70%, 50%)",
      data: [
        { x: "09:00", y: 55 },
        { x: "10:00", y: 51 },
        { x: "11:00", y: 59 },
        { x: "12:00", y: 147 },
        { x: "13:00", y: 15 },
        { x: "14:00", y: 212 },
        { x: "15:00", y: 192 },
        { x: "16:00", y: 137 },
        { x: "17:00", y: 101 },
        { x: "18:00", y: 286 },
        { x: "19:00", y: 153 },
        { x: "20:00", y: 91 },
      ],
    },
  ]);
};
