// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

export default (req, res) => {
  res.status(200).json([
    {
      id: "1",
      name: "Average Dwell Time",
      measureUnit: "minutes",
      measureUnitAbbrev: "mins",
      valueUnit: "",
      avgVal: "21",
      avgValTrend: "-5",
      avgValTrendPercentage: "-24",
      updatedAt: "2021-06-04T02:00:01.095Z",
      graphData: [
        {
          id: "Average Dwell Time",
          color: "hsl(38.2, 100%, 50%)",
          data: [
            {
              x: "Monday",
              y: 13,
            },
            {
              x: "Tuesday",
              y: 18,
            },
            {
              x: "Wednesday",
              y: 12,
            },
            {
              x: "Thursday",
              y: 11,
            },
            {
              x: "Friday",
              y: 14,
            },
            {
              x: "Saturday",
              y: 21,
            },
            {
              x: "Sunday",
              y: 25,
            },
          ],
        },
      ],
    },
    {
      id: "2",
      name: "Advertising Trend",
      measureUnit: "millions",
      measureUnitAbbrev: "m",
      valueUnit: "£",
      avgVal: "180",
      avgValTrend: "+10",
      avgValTrendPercentage: "12",
      updatedAt: "2021-06-04T02:00:01.095Z",
      graphData: [
        {
          id: "Advertising Trend",
          color: "hsl(38.2, 100%, 50%)",
          data: [
            {
              x: "Monday",
              y: 132,
            },
            {
              x: "Tuesday",
              y: 181,
            },
            {
              x: "Wednesday",
              y: 223,
            },
            {
              x: "Thursday",
              y: 234,
            },
            {
              x: "Friday",
              y: 412,
            },
            {
              x: "Saturday",
              y: 212,
            },
            {
              x: "Sunday",
              y: 382,
            },
          ],
        },
      ],
    },
    {
      id: "3",
      name: "Facemask Compiliance",
      measureUnit: "percentage",
      measureUnitAbbrev: "%",
      valueUnit: "",
      avgVal: "49",
      avgValTrend: "-11",
      avgValTrendPercentage: "-11",
      updatedAt: "2021-06-04T02:00:01.095Z",
      graphData: [
        {
          id: "Facemask Compiliance",
          color: "hsl(38.2, 100%, 50%)",
          data: [
            {
              x: "Monday",
              y: 33,
            },
            {
              x: "Tuesday",
              y: 38,
            },
            {
              x: "Wednesday",
              y: 57,
            },
            {
              x: "Thursday",
              y: 59,
            },
            {
              x: "Friday",
              y: 46,
            },
            {
              x: "Saturday",
              y: 45,
            },
            {
              x: "Sunday",
              y: 32,
            },
          ],
        },
      ],
    },
    {
      id: "4",
      name: "Customers Onsite",
      measureUnit: "people",
      measureUnitAbbrev: "people",
      valueUnit: "",
      avgVal: "3220",
      avgValTrend: "-1100",
      avgValTrendPercentage: "-35",
      updatedAt: "2021-06-04T02:00:01.095Z",
      graphData: [
        {
          id: "CUSTOMERS ONSITE",
          color: "hsl(38.2, 100%, 50%)",
          data: [
            {
              x: "Monday",
              y: 2334,
            },
            {
              x: "Tuesday",
              y: 1418,
            },
            {
              x: "Wednesday",
              y: 1234,
            },
            {
              x: "Thursday",
              y: 1568,
            },
            {
              x: "Friday",
              y: 2100,
            },
            {
              x: "Saturday",
              y: 2123,
            },
            {
              x: "Sunday",
              y: 2555,
            },
          ],
        },
      ],
    },
  ]);
};
