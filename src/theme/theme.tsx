import { createMuiTheme } from "@material-ui/core/styles";
import { red, orange } from "@material-ui/core/colors";
import { green100 } from "material-ui/styles/colors";

// Create a theme instance.
const theme = createMuiTheme({
  palette: {
    primary: {
      main: orange.A400,
    },
    secondary: {
      main: "#ef5350",
    },
    error: {
      main: red.A400,
    },
    success: {
      main: "#50D050",
    },
    warning: {
      main: "#ef5350",
    },
    background: {
      default: "#F5F5F5",
    },
  },
});

export default theme;
