import React, { createContext, useEffect, useState } from "react";
import { setCookie, parseCookies, destroyCookie } from "nookies";
import Router from "next/router";
import moment from "moment";
import axios from "axios";

type User = {
  id: string | string[];
  email: string | string[];
};

type SignInData = {
  code: string | string[];
  state: string | string[];
};

type AuthContextType = {
  isAuthenticated: boolean;
  user: User;
  signIn: (data: SignInData) => Promise<void>;
  getUserInfo: () => Promise<void>;
  requestSignIn: () => Promise<void>;
  refreshTokenNow: () => Promise<void>;
  logout: () => Promise<void>;
  checkTokenUpdateNecessary: () => Promise<void>;
};

export const AuthContext = createContext({} as AuthContextType);

export function AuthProvider({ children }) {
  const [user, setUser] = useState<User | null>(null);
  const isAuthenticated = !!user;

  const apiOauth = axios.create({
    baseURL: "https://sabre-oauth.mova.ie", //TODO Add env var
    withCredentials: true,
    timeout: 3000,
  });

  /**
   * Encode the parameter object to be sent by query params.
   * @param {object} params - Parameters object.
   * @returns {string} Returns string with query params.
   */
  const encodeParams = (params: object) =>
    Object.keys(params)
      .map(
        (key) => `${encodeURIComponent(key)}=${encodeURIComponent(params[key])}`
      )
      .join("&");

  /**
   * Create the authorization header.
   * @param {boolean} auth - Use true to send a Basic Token. Default = false.
   * @returns {object} Returns the authorization header.
   */
  const createAuthorizationHeader = (auth = false) => {
    if (!auth) return { "Content-Type": "application/x-www-form-urlencoded" };
    const client_id = "myClientId"; //TODO Add env var
    const client_secret = "MYSECRET"; //TODO Add env var
    const token = window.btoa(`${client_id}:${client_secret}`);
    return {
      "Content-Type": "application/x-www-form-urlencoded",
      Authorization: `Basic ${token}`,
    };
  };

  /**
   * Auxiliary function. Store tokens in the cookie.
   * @param {string} access_token - Access token returned from oauth server.
   * @param {string} expires_in - Expires in returned from oauth server.
   * @param {string} refresh_token - Refresh token returned from oauth server.
   */
  const updateAuthCookies = (
    access_token: string,
    expires_in: string | number,
    refresh_token: string
  ) => {
    setCookie(undefined, "nextauth.token", access_token, {
      maxAge: 3 * 30 * 24 * 60 * 60, //Three months to expire
    });
    const expiration_date = moment()
      .add(Number(expires_in) - 30, "s")
      .format(); // Subtracted 30 seconds from the token's validity, to be replaced before it expires.
    setCookie(undefined, "nextauth.expires_in", expiration_date, {
      maxAge: 3 * 30 * 24 * 60 * 60, //Three months to expire
    });
    setCookie(undefined, "nextauth.refresh_token", refresh_token, {
      maxAge: 3 * 30 * 24 * 60 * 60, //Three months to expire
    });
  };

  /**
   * Auxiliary function. Removes tokens saved in the cookie.
   */
  const destroyAuthCookies = () => {
    destroyCookie(undefined, "nextauth.token");
    destroyCookie(undefined, "nextauth.expires_in");
    destroyCookie(undefined, "nextauth.refresh_token");
  };

  useEffect(() => {
    const { "nextauth.token": token } = parseCookies();
    const { "nextauth.refresh_token": refreshToken } = parseCookies();
    const { "nextauth.expires_in": expires_in } = parseCookies();

    // THEN THINKING ABOUT HOW TO MAKE THE REFRESH TOKEN LOGIC
    // if (token && refreshToken && !user) getUserInfo();
    if (token && refreshToken && expires_in) checkTokenUpdateNecessary();
  }, []);

  /**
   * Redirects the user to the login page on the authentication server.
   */
  async function requestSignIn() {
    const params = {
      client_id: "myClientId", //TODO Add env var
      redirect_uri: "http://localhost:3003/callback", //TODO Add env var
      response_type: "code",
      grant_type: "authorization_code",
      state: "myState",
    };
    const body = encodeParams(params);
    window.location.href = `https://sabre-oauth.mova.ie/oauth/authorize?${body}`; //TODO Add env var
  }

  /**
   * Replace the authorization code with a token and refresh token from the oauth server.
   * You must have a valid authorization code.
   * Tokens are stored in the cookie and the user is redirected to the main page.
   * User information is updated.
   * @param {string} code - Code returned from oauth server.
   * @param {string} state - State returned from oauth server.
   */
  async function signIn({ code, state }: SignInData) {
    const params = {
      code,
      grant_type: "authorization_code",
      redirect_uri: "http://localhost:3003/callback", //TODO Add env var
    };
    const body = encodeParams(params);
    const response = await apiOauth.post("oauth/token", body, {
      headers: createAuthorizationHeader(true), //True for add barear basic
    });
    const { access_token, refresh_token, expires_in } = response.data;
    updateAuthCookies(access_token, expires_in, refresh_token);
    getUserInfo();
    Router.push("/");
  }

  /**
   * Get the user information.
   * Before, it is necessary to have an active session with the oauth server.
   * User information is stored in context state.
   */
  async function getUserInfo() {
    const response = await apiOauth.get("userinfo", {
      headers: createAuthorizationHeader(),
    });
    const { sub, email } = response.data;
    setUser({
      id: sub,
      email: email,
    });
  }

  /**
   * Request a new token and refresh token from the authentication server.
   * You must have a valid refresh token.
   * Tokens are stored in the cookie.
   */
  async function refreshTokenNow() {
    const { "nextauth.refresh_token": refreshToken } = parseCookies();
    const params = {
      code: refreshToken,
      grant_type: "refresh_token",
      redirect_uri: "http://localhost:3003/callback", //TODO Add env var
    };
    const body = encodeParams(params);
    const response = await apiOauth.post("oauth/token", body, {
      headers: createAuthorizationHeader(true), //True for add barear basic
    });
    const { access_token, refresh_token, expires_in } = response.data;
    updateAuthCookies(access_token, expires_in, refresh_token);
  }

  /**
   * Remove access authorization.
   * Before, it is necessary to have an active session with the oauth server.
   * Tokens are removed from the cookie.
   * User information is removed from context state.
   * The user is redirected to the main page.
   */
  async function logout() {
    await apiOauth.get("oauth/logout", {
      headers: createAuthorizationHeader(),
    });
    destroyAuthCookies();
    setUser({
      id: null,
      email: null,
    });
    Router.push("/");
  }

  /**
   * Checks if the token exists and if it is necessary to update with a new one,
   * if it has expired.
   * It is recommended that this function be called before requests to the resource server.
   */
  async function checkTokenUpdateNecessary() {
    console.log("CHECK");

    const { "nextauth.token": token } = parseCookies();
    if (!token) return await logout();
    const { "nextauth.expires_in": expires_in } = parseCookies();
    const isUpdateNecessary = moment().isAfter(expires_in);
    if (isUpdateNecessary) return await refreshTokenNow();
  }

  return (
    <AuthContext.Provider
      value={{
        user,
        isAuthenticated,
        signIn,
        getUserInfo,
        requestSignIn,
        refreshTokenNow,
        logout,
        checkTokenUpdateNecessary,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
}
