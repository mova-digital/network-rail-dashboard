import useSWR from "swr";
import * as api from "../services/api";

/**
 * This is the entity for the index page to give an overview of each station.
 * The return data will cover the overview data for each station for number of devices, ads etc.
 * @returns {object} Returns overview data all stations.
 */
export const StationsListGeneralData = () =>
  useSWR("StationsListGeneralData", async () => {
    const response = await api.StationsListGeneralData();
    return response.returnData;
  });

/**
 * This is the entity for when a station has been selected for detailed view.
 * The return data will cover the overview data for each area in the station for number of devices, ads etc.
 * @param {string} stationId - Station id.
 * @returns {object} Returns overview data for each area in the station.
 */
export const StationDetails = (stationId) =>
  useSWR("StationDetails", async () => {
    const response = await api.StationDetails(stationId);
    return response.returnData;
  });

/**
 * This is the entity for the index page that will return overview data for the graphs etc.
 * Averaging across all stations.
 * @returns {object} Returns average data all stations.
 */
export const AverageChartAllStations = () =>
  useSWR("AverageChartAllStations", async () => {
    const response = await api.AverageChartAllStations();
    console.log("response.returnData", response.returnData);
    return response.returnData;
  });

/**
 * This is the entity for when a station has been selected for detail.
 * The return data will cover the overview data for the graphs etc. for the station.
 * @param {string} stationId - Station id.
 * @returns {object} Returns average data from corresponding station.
 */
export const AverageChartOneStation = (stationId) =>
  useSWR("AverageChartOneStation", async () => {
    const response = await api.AverageChartOneStation(stationId);
    console.log("response.returnData", response.returnData);
    return response.returnData;
  });

/**
 * This is the entity for when an area has been selected for detail.
 * The return data will cover the overview data for the graphs etc. for the area.
 * @param {string} areaId - Area id.
 * @returns {object} Returns data from corresponding area.
 */
export const AverageChartOneArea = (areaId) =>
  useSWR("AverageChartOneArea", async () => {
    const response = await api.AverageChartOneArea(areaId);
    return response.returnData;
  });

/**
 * This is the entity for searching for a specific datapoint, in a given timeframe, for stations with a given name value.
 * @param {object[]} filters - Check the filter format in the api documentation.
 * @returns {object} Returns data from corresponding stations.
 */
export const SearchByStation = (filters) =>
  useSWR("SearchByStation", async () => {
    const response = await api.SearchByStation(filters);
    return response.returnData;
  });

/**
 * This is the entity for all the areas in a station when filtering for a specific datapoint.
 * @param {object[]} filters - Check the filter format in the api documentation.
 * @returns {object} Returns data from corresponding areas.
 */
export const SearchForAreasOfStation = (filters) =>
  useSWR("SearchForAreasOfStation", async () => {
    const response = await api.SearchForAreasOfStation(filters);
    return response.returnData;
  });
